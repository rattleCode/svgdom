 ########################################################################
 #
 #  This software is copyrighted by Manfred Rosenberger and other parties.  
 #  The following terms apply to all files associated with the software unless
 #  explicitly disclaimed in individual files.
 #  
 #  The authors hereby grant permission to use, copy, modify, distribute,
 #  and license this software and its documentation for any purpose, provided
 #  that existing copyright notices are retained in all copies and that this
 #  notice is included verbatim in any distributions. No written agreement,
 #  license, or royalty fee is required for any of the authorized uses.
 #  Modifications to this software may be copyrighted by their authors
 #  and need not follow the licensing terms described here, provided that
 #  the new terms are clearly indicated on the first page of each file where
 #  they apply.
 #  
 #  IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 #  FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 #  ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
 #  DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
 #  POSSIBILITY OF SUCH DAMAGE.
 #  
 #  THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 #  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 #  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
 #  IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
 #  NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 #  MODIFICATIONS.
 #  
 #  GOVERNMENT USE: If you are acquiring this software on behalf of the
 #  U.S. government, the Government shall have only "Restricted Rights"
 #  in the software and related documentation as defined in the Federal 
 #  Acquisition Regulations (FARs) in Clause 52.227.19 (c) (2).  If you
 #  are acquiring the software on behalf of the Department of Defense, the
 #  software shall be classified as "Commercial Computer Software" and the
 #  Government shall have only "Restricted Rights" as defined in Clause
 #  252.227-7013 (c) (1) of DFARs.  Notwithstanding the foregoing, the
 #  authors grant the U.S. Government and others acting in its behalf
 #  permission to use and distribute the software in accordance with the
 #  terms specified in this license.
 # 
 # ----------------------------------------------------------------------
 #
 #  Copyright (c) Manfred ROSENBERGER, 2018
 #
 # ----------------------------------------------------------------------
 #  http://www.magicsplat.com/articles/oo.html
 #  https://wiki.tcl.tk/25998
 # ----------------------------------------------------------------------
 #  
 #  other sources:
 #
 #  svgDOM.tcl -- an oo hierarchy to parse, mutate and generate SVG
 #      2018.05.23
 #
 #          https://wiki.tcl.tk/25998
 #          https://wiki.tcl.tk/3650?redir=3654  -> Colin McCormack 
 #
 #  -------
 #
 #  Canvas geometrical calculations
 #          kvetter@DELETETHIS.alltel.net
 #          http://wiki.tcl.tk/8447
 #  
 #  -------
 #
 #       http://www.magicsplat.com/articles/oo.html
 #
 #       https://www.w3.org/TR/SVG/coords.html#TransformAttribute
 #       https://nikit.tcl-lang.org/page/svg2can
 #
 # ----------------------------------------------------------------------
 # 
 #  2018/05/26
 #      ... extracted from the rattleCAD-project (Version 3.4.05) by
 #          the author of rattleCAD
 #
 # ----------------------------------------------------------------------
 #  namespace:  svgDOM
 # ----------------------------------------------------------------------
 #
 #  0.07 - 2017/12/09
 #      ... smaller cleanups
 #  0.08 - 2019/10/12
 #      ... read from inkscape
 #  0.08.03 - 2023/07/30
 #      ... ellipse -> circle: radius accuracy: 0.3f 
 #  0.08.03 - 2023/07/30
 #      ... ellipse -> circle: radius accuracy: 0.3f 
 #  0.09.01 - 2024/02/01
 #      ... path -> native support 
 #  0.09.02 - 2024/06/28
 #      ... circle, ellipse, rectangle: remove -transform 
 #
 
    #
package provide svgDOM 0.09.02
    #
package require TclOO
package require tdom
package require fileutil
package require math            1.2.5
package require math::geometry

package require dicttool


namespace eval svgDOM {
        #
    oo::class create Element
    oo::class create SVGShape
    oo::class create SVGPath
        #
    oo::class create SVGMatrix
    oo::class create SVGStyle
        #
    oo::class create SVG
    oo::class create SVGElement
        #
    oo::class create Circle
    oo::class create Ellipse
    oo::class create Line
    oo::class create Path
    oo::class create PathFraction
    oo::class create Polygon
    oo::class create Polyline
    oo::class create Rectangle    
        #
}