 #
 # svgDOM
 #
 # https://www.w3.org/TR/SVG/shapes.html
 #
 #  2018.05.30 
 #
 #

oo::define svgDOM::Circle {
        #
        # https://www.w3.org/TR/SVG/shapes.html
        #
        # rect 
        # circle 
        # ellipse 
        # line 
        # polyline 
        # polygon         
        #
        #
    variable attrList matrixObj resolution
    variable pointList
    variable shapeDict               ;# the returning node definition
        #
        #
    superclass svgDOM::Ellipse       ;# circle is a special ellipse ;) 
        #
        #
    constructor {_attrList _matrixObj {_resolution 1}} {
            #
        set attrList    $_attrList
        set matrixObj   $_matrixObj
        set resolution  $_resolution
            #
            # puts "            -> Circle:    $attrList"
            # puts "                          $matrix"
            #
        set pointList   {}
        set shapeDict   {}
            #
            # puts "    -> $attrList"
        set r       [dict get $attrList r]
            # puts "    -> r $r"
        dict set     attrList rx $r
        dict set     attrList ry $r
            # puts "    -> $attrList"
            #
        next    $attrList $matrixObj
            #
    }
        #
    destructor { 
        puts "            -> [self] ... destroy SVGShape"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::Circle::$target_method $args  ... unknown"
    }
        #
}        