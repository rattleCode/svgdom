 #
 # svgDOM
 #
 # https://www.w3.org/TR/SVG/shapes.html
 #
 #  2018.05.30 
 #
 #

oo::define svgDOM::Path {
        #
        # https://www.w3.org/TR/SVG/shapes.html
        #
        # rect 
        # circle 
        # ellipse 
        # line 
        # polyline 
        # polygon         
        #
    variable attrList matrixObj
    variable polygon polyline
    variable shapeDict                  ;# the returning node definition
        #
    variable pathDefinition
    variable pathDef_Normalized
    variable pathDef_Absolute
    variable pathDef_Splited
        #
    variable svgPrecision
        #
    variable _shapeDict
        #
    superclass svgDOM::SVGPath          ;# the template class of every element
        #
    constructor {_attrList _matrixObj} {
            #
            # puts "\n<I>\n<I> ... svgDOM::Path\n<I>"
            #
        set svgPrecision    1    
            #
        set attrList    $_attrList
        set matrixObj   $_matrixObj
            #
            # puts "            -> Path:      $attrList"
            # puts "                          $matrix"
            #
            #
        set pointList   {}
        set _shapeDict  {}
        set shapeDict   {}
            #
        next $_attrList $_matrixObj
            #
        set pathDefinition      [dict get $attrList d]
            #
        set pathDef_Normalized  [my NormalizePath   $pathDefinition]
            #
        # puts "                  -> pathDefinition:       -> $pathDefinition"    
        # puts "                  -> pathDef_Normalized:   -> $pathDef_Normalized"    
        # puts "                  -> polyline:  -> $polyline"    
            #
    }
        #
    destructor { 
        puts "            -> [self] ... destroy svgDOM::Path"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::Path::$target_method $args  ... unknown"
    }
        #
    method get {} {
            #
            # puts "\n === Path -> get ===\n"
            #
            # puts "\n  --- Scale --- \n"
            # puts "        \$styleScale -> [$matrixObj get]"
            #
        set pathDef_Absolute    [my AbsolutePath    $pathDef_Normalized]
            #
            # puts "        -> \$pathDef_Absolute: $pathDef_Absolute"
            #
            #
        set pathDef_Scaled      [my ScalePath       $pathDef_Absolute]
            #
            # puts "        -> \$pathDef_Scaled:  "
            # puts [dict print $pathDef_Scaled]
            #
            # puts "  -------> \$pathDef_Scaled:"
            # puts "[dict print $pathDef_Scaled]"
            #
        if {$pathDef_Scaled ne {}} {
            return $pathDef_Scaled
        } else {
            return {}
        }
            #
    }
        #
    method ScalePath {pathDef} {
            #
        set shapeDict   {}  ; # will become shapeDict in future ...
            #
            #
            # puts "\n == SplitPath ===========================\n\n  -> $pathDef"
            #
            #
        if {[dict exist $attrList id]} {
            set nodeID  [dict get $attrList id]
        } else {
            set nodeID  [clock microseconds]
        }
            #
            # puts "    \$pathDef $pathDef"    
            #
            # puts "  -> \$__shapeDict   ->  $__shapeDict"
            # puts "  -> \$pathDef   ->  $pathDef"
            #
        set indexList       [lsearch -exact -all $pathDef M]
        lappend indexList   [llength $pathDef]    ;# $i is not an index in the list but a limiter
            # puts "  -> \$indexList   ->  $indexList"
            # puts "       -> [lindex $pathDef [llength $pathDef]-2]"
            # puts "       -> [lindex $pathDef [llength $pathDef]-1]"
            #
        set segmentList   {}    
            # set __splitList {}
            #
        set i 0
            #
        foreach j [lrange $indexList 1 end] {
                # puts "      b $i -> $j"
                # continue
            set pathSegment [lrange $pathDef $i $j-1]
                # puts "    -> \$pathSegment $pathSegment"
            lappend segmentList $pathSegment
            set i $j
        }
            #
            #
            # puts "\n ------------------------------------------"
            # puts "  - 000 -> \$pathDef      -> $pathDef"
            # puts "  - 000 -> \$segmentList  <- [llength $segmentList]"
            # foreach segment $segmentList {
            #     puts "          -> $segment"
            # }
            #
            #
        set list_PathClosed {}
        set list_PathOpen   {}
            #
        set pathClosed  {}
            #
        foreach segment $segmentList {
                # puts "     - 002 -> \$segment $segment"
            set closeNode   [lindex [join $segment] end]
                # puts "         --> \$closeNode $closeNode\n"
            if {$closeNode == {Z}} {
                lappend list_PathClosed $segment ;# [lindex $segment 0] [lrange $segment 1 end-1] [lindex $segment end]
                set pointList           [my CreatePointList  $segment]
                lappend pathClosed      M $pointList Z                
            } else {
                lappend list_PathOpen   [my CreatePointList  $segment];# $segment ;# [lindex $segment 0] [lrange $segment 1 end]
            }
        }
            #
            # puts "  - 004 -> \$pathClosed"
        set pathClosed      [join $pathClosed]
            # puts "              -> $pathClosed"
            #
            #
            # -- build result Dict ---
            #
        set pathDict    {}
            # dict set pathDict g [list id $nodeID] 
            #
        set i 0
            #
        if {$pathClosed != {}} {
                dict set pathDict \
                     $i [list \
                            path \
                                [list \
                                        d   $pathClosed \
                                    ] \
                                ]
        }
            #
        if {$list_PathOpen != {}} {
            foreach line $list_PathOpen {
                incr i
                dict set pathDict \
                     $i [list \
                            polyline \
                                [list \
                                    points  $line \
                                    ] \
                                ]
            }
        }
            #
            #
        # puts "[dict print $pathDict]"
            #
            #
        if {$pathDict == {}} {
            return {}
        }
            #
        set resultDict {}
        dict set resultDict type      group
            #
        dict set resultDict content   id    $nodeID
        dict set resultDict content   g     $pathDict
            #
        return $resultDict
            #
            #
    }
        #
}        