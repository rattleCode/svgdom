 #
 # svgDOM
 #
 # https://www.w3.org/TR/SVG/shapes.html
 #
 #  2018.05.30 
 #
 #

oo::define svgDOM::Polygon {
        #
        # https://www.w3.org/TR/SVG/shapes.html
        #
        # rect 
        # circle 
        # ellipse 
        # line 
        # polyline 
        # polygon         
        #
        #
    variable attrList matrix    
    variable pointList
    variable shapeDict                  ;# the returning node definition
        # variable polyline
        #
        #
    superclass svgDOM::SVGShape         ;# the template class of every element
        #
        #
    constructor {_attrList _matrix} {
            #
        set attrList    $_attrList
        set matrix      $_matrix
            #
        # puts "            -> Polygon:   $attrList"
        # puts "                          $matrix"
            #
        set pointList   {}
        set shapeDict   {}
            #
        next            polygon     $attrList $matrix
            #
        my parseShape    
        my toShapeDict
            #
        # puts "                  -> pointList:   -> $pointList"    
            #
    }
        #
    destructor { 
        puts "            -> [self] ... destroy SVGShape"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::Polygon::$target_method $args  ... unknown"
    }
        #
    method parseShape {} {
            #
            # puts "   -> $attrList"
            #
        set points      [dict get $attrList points]
            #
            # puts "        -> $points"    
        set pointList   [string map {, { }} $points]
            # puts "        -> $polyline"    
        set pointList   [my transform $pointList]    
            #
    }
        #
    method toShapeDict {} {
            #
        set dictStore   [dict remove $attrList points transform]
            #
        set _pointList {}
        foreach  {x y} $pointList {
            lappend _pointList "$x,$y"
        }
        set _pointList  [join $_pointList { }]
            #
            # set dictAttr    [dict merge $dictStore [list points $_pointList]]    
            #
        set shapeDict   [list polygon [list points $_pointList]]
            #
    }
        #
}        