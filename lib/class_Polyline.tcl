 #
 # svgDOM
 #
 # https://www.w3.org/TR/SVG/shapes.html
 #
 #  2018.05.30 
 #
 #

oo::define svgDOM::Polyline {
        #
        # https://www.w3.org/TR/SVG/shapes.html
        #
        # rect 
        # circle 
        # ellipse 
        # line 
        # polyline 
        # polygon         
        #
        #
    variable attrList matrixObj
    variable pointList
    variable shapeDict                  ;# the returning node definition
        #
        #
    superclass svgDOM::SVGShape         ;# the template class of every element
        #
        #
    constructor {_attrList _matrixObj} {
            #
        set attrList    $_attrList
        set matrixObj   $_matrixObj
            #
            # puts "            -> Polyline:  $attrList"
            # puts "                          $matrix"
            #
        set pointList   {}
        set shapeDict   {}
            #
        next            polyline    $attrList $matrixObj
            #
        my parseShape
        my toShapeDict
            #
            # puts "                  -> pointList:   -> $pointList"    
            #
    }
        #
    destructor { 
        puts "            -> [self] ... destroy SVGShape"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::Polyline::$target_method $args  ... unknown"
    }
        #
    method parseShape {} {
            #
            # puts "   -> $attrList"
            #
        set points      [dict get $attrList points]
            #
            # puts "        -> $points"    
        set pointList   [string map {, { }} $points]
            # puts "        -> $pointList"    
        set pointList   [my transform $pointList]    
            #
    }
        #
    method toShapeDict {} {
            #
        set dictStore   [dict remove $attrList points transform]
            #
        set _pointList {}
        foreach  {x y} $pointList {
            lappend _pointList "$x,$y"
        }
        set _pointList  [join $_pointList { }]
            #
            # set dictAttr    [dict merge $dictStore [list points $_pointList]]    
            #
        set shapeDict   [list polyline [list points $_pointList]]
            #
    }
        #
        
        
        
}        