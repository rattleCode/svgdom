 #
 # svgDOM
 #
 # https://www.w3.org/TR/SVG/shapes.html
 #
 #  2018.05.30 
 #
 #

oo::define svgDOM::Rectangle {
        #
        # https://www.w3.org/TR/SVG/shapes.html
        #
        # rect 
        # circle 
        # ellipse 
        # line 
        # polyline 
        # polygon         
        #
        #
    variable attrList matrixObj
    variable pointList
    variable shapeDict                   ;# the returning node definition
        # polyline
        #
        #
    superclass svgDOM::SVGShape         ;# the template class of every element
        #
        #
    constructor {_attrList _matrixObj} {
            #
        set attrList    $_attrList
        set matrixObj   $_matrixObj
            #
            # puts "            -> Rectangle: $attrList"
            # puts "                          $matrix"
            #
        set pointList   {}
        set shapeDict    {}    
            #
        next            rect    $attrList $_matrixObj
            #
        my parseShape
        my toShapeDict
            #
            # puts "                  -> polygon:   -> $polygon"    
            #
    }
        #
    destructor { 
        puts "            -> [self] ... destroy SVGShape"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::Rectangle::$target_method $args  ... unknown"
    }
        #
    method parseShape {} {
            #
            # puts "   -> $attrList"
            #
        set x   0
        set y   0
            #
        if {[dict exists $attrList x]} {
            set x   [dict get $attrList x]
        }
        if {[dict exists $attrList y]} {
            set y   [dict get $attrList y]
        }
            #
        set width   [dict get $attrList width]
        set height  [dict get $attrList height]
            #
        set pointList \
                [list \
                    $x                      $y \
                    [expr {$x + $width}]    $y \
                    [expr {$x + $width}]    [expr {$y + $height}] \
                    $x                      [expr {$y + $height}] \
                ]
            #
        set pointList   [my transform $pointList]    
            #
            # puts "\n-- rectangle -- \$attrList   $attrList\n"
        set attrList    [dict remove $attrList   width height transform]
            #
            # puts "\n-- rectangle -- \$attrList   $attrList\n"
            # puts "\n-- rectangle -- \$pointList  $pointList\n"
            #
    }
        #
    method toShapeDict {} {
            #
        set dictStore   [dict remove $attrList x y width height transform]
            #
        set _pointList {}
        foreach  {x y} $pointList {
            lappend _pointList "$x,$y"
        }
        set _pointList  [join $_pointList { }]
            #
            # set dictAttr    [dict merge $dictStore [list points $_pointList]]    
            #
        set shapeDict    [list polygon [list points $_pointList]]
            #
    }    
        #   
}        