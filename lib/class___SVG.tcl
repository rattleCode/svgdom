#
# SVG.tcl -- an oo hierarchy to parse, mutate and generate SVG
#   2018.05.23
#
# https://wiki.tcl.tk/25998
# https://wiki.tcl.tk/3650?redir=3654  -> Colin McCormack
#
#

oo::define svgDOM::SVG {
        #
    variable doc rootNode styleNode
    variable matrix style
    variable u_id dict_id2obj children styleScale
    variable pathType
    variable flattenMode
    variable expDoc
        #
    variable keepDoc
        #
    superclass svgDOM::Element        ;# the document is itself an element
        #
    constructor {args} {
            #
        set u_id        0   ;# unique id in object structure
        set dict_id2obj {}  ;# map id->object
        set children    {}
        set styleScale  1
            #
        set pathType    fraction    ;# [fraction/default]
        set flattenMode deep        ;# [deep/default]
            #
        set keepDoc     0
            # --- parse arguments
            #
        if {[dict exists $args file]} {
                #
                # create document from xml text in named file
                #
            package require fileutil
            set xml [::fileutil::cat [dict get $args file]]
            set doc [dom parse $xml]
                #
        } elseif {[dict exists $args svgNode]} {
                #
                # get document from root <svg> node
                #
            set svgNode     [dict get $args svgNode]
            set keepDoc     1
            set doc [$svgNode ownerDocument]
                #
        } elseif {[dict exists $args xml]} {
                #
                # create a document from xml text
                #
            set doc [dom parse [dict get $args xml]]
                #
        } else {
                #
                # create an empty document
                #
            set doc [dom createDocument svg]
                #
        }
            #
        set templateDoc [dom parse [$doc asXML]]
            #
            #
            # --- get the root node <svg>
            #
        set rootNode    [$doc documentElement] 
            #
            #
            #
            # puts "    -> [$rootNode asXML]"
            #
        if {[$rootNode nodeType] eq "ELEMENT_NODE"} {
                #
            next [self] [self] $rootNode   ; # construct [self] as an element
                #
            foreach childNode [$rootNode childNodes] {
                    # puts "  ---> $childNode -> [$childNode getAttribute id]"
                switch -exact -- [$childNode nodeName] {    
                    g -
                    rect -
                    circle -
                    ellipse -
                    line -
                    polyline -
                    polygon -
                    path {
                            #
                            # puts "    -> $childNode"
                            #
                        set obj [my buildChild [self] [self] $childNode]
                        if {$obj ne {}} {
                            lappend children $obj
                        }
                    }
                    default {}
                } 
            }
                #
            return [self]
                #
        }
            #
            #
            # --- exception
            #
        return {}
            #
    }        
        #
    destructor {
        if {!$keepDoc} {
            $doc delete     ;# delete the tdom parsed doc
        }
        next                ;# delete the corresponding doc element
    }
        #
    method update {} {
            #
        next    
            #
        set style   [svgDOM::SVGStyle  new]
            #
        set matrix  [svgDOM::SVGMatrix new]    
            #
        lassign     [my viewScale]     m styleScale
            #
            # puts "   -> \$m             $m"   
            # puts "   -> \$styleScale    $styleScale"   
            #
        $matrix multiply $m    
            #
    }
        #
        # we maintain a document-wide map of id->object.
        # id is just an attribute of elements
        # id is guaranteed unique within documents by means of this map.        
        #
    method set_id2obj {id obj} {
        dict set dict_id2obj $id $obj
    }
        #
    method del_id2obj {id} {
        dict unset dict_id2obj $id
    }
        #
    method get_by_id {id} {
        return [dict get $id2obj $id]
    }        
        #
    method pathType {{type {}}} {
        if {${type} eq {}} {
            return $pathType
        } else {
            switch -exact -- $type {
                fraction {
                    set pathType $type      ;# splits a path into polylines and polygons for use in tk:canvas 
                }
                path {
                    set pathType $type      ;# keeps path as path for use in tkpath 
                }
                default {
                    set pathType fraction
                }
            }
        }
    }
        #
    method flattenMode {{type {}}} {
        if {${type} eq {}} {
            return $flattenMode
        } else {
            switch -exact -- $type {
                deep {
                    set flattenMode $type      ;# keep <g> with just one child 
                }
                flat {
                    set flattenMode $type      ;# move single child of <g> to parent node and remove this <g>
                }
                default {
                    set flattenMode flat
                }
            }
        }
    }
        #
    method exists {id} {
        if {[dict exists $dict_id2obj $id]} {
            return 1
        } else {
            set checkNode [$doc getElementById $id]
                # puts "  -> exists: \$checkNode $checkNode"
            if {$checkNode != {}} {
                return 1
            } else {
                return 0
            }
        }
    }
        #
    method unique_id {{id {}}} {
            #
        # set id [format {_uid_%s} [incr u_id]]
        # return $id
            #
            #
        if {$id != {}} {
            set checkNode [$doc getElementById $id]
                # puts "      -> \$id $id   -> count: [llength $checkNode] "
            if {[llength $checkNode] > 1} {
                set id [format {_uid_%s} [incr u_id]]
            }
        } else {
            set id [format {_uid_%s} [incr u_id]]
        }
            # puts "   -> unique_id ... \$u_id $u_id  -> $id"
        if {[my exists $id] >= 1} {
            # puts "       -> exists:   $id -?- [my exists $id]"
            while 1 {
                set id [format {_uid_%s} [incr u_id]]
                    # puts "            incr -> \$id $id"
                if {[my exists $id] == 0} {
                    # puts "       -> exists:   $id -?- [my exists $id]"
                    return $id
                }
            }
        }
        return $id
    }
        #
        #
    method getExpDoc {} {
        return $expDoc
    }
        #
    method viewScale {} {
            #
            #
        set styleScale  1    
            #
        set viewWidth   0
        set viewHeight  0
            #
        if {[catch {set viewWidth   [$rootNode getAttribute width]}     eID]} {set viewWidth    {}}
        if {[catch {set viewHeight  [$rootNode getAttribute height]}    eID]} {set viewHeight   {}}
        if {[catch {set viewBox     [$rootNode getAttribute viewBox]}   eID]} {set viewBox      {}}
            #
            # puts "       -> \$viewWidth     $viewWidth"
            # puts "       -> \$viewHeight    $viewHeight"
            # puts "       -> \$viewBox       $viewBox"
            #
        if {$viewBox eq {}} {
            return {{1 0 0 1 0 0} 1}
        } else {
                #
            lassign $viewBox    x1 y1 viewBoxWidth viewBoxHeight
                # puts "       -> \$viewBoxWidth  $viewBoxWidth"
                # puts "       -> \$viewBoxHeight $viewBoxHeight"
                #
            if {$viewWidth eq {} && $viewHeight eq {}} {
                return {{1 0 0 1 0 0} 1}
            }
                #
            if {$viewWidth eq {}} {
                set sx  1
            } else {
                set _unit       [regsub -all -- {[0-9]} $viewWidth ""]
                set viewWidth   [regsub -all -- {[a-zA-Z%]} $viewWidth ""]
                switch -exact $_unit {
                    {%} {
                        set sx  [expr {0.01 * $viewWidth}]
                    }
                    default {
                        set sx  [expr {1.0 * $viewWidth / $viewBoxWidth}]
                    }
                }
                    # puts "   -- $sx <- $viewWidth / $_unit / $viewBoxWidth"
            }
                # 
            if {$viewHeight eq {}} {
                set sy  1
            } else {
                    #
                set _unit       [regsub -all -- {[0-9]} $viewHeight ""]
                set viewHeight  [regsub -all -- {[a-zA-Z%]} $viewHeight ""]
                switch -exact $_unit {
                    {%} {
                        set sy  [expr {0.01 * $viewHeight}]
                    }
                    default {
                        set sy  [expr {1.0 * $viewHeight / $viewBoxHeight}]
                    }
                }
                    # puts "   -- $sy <- $viewHeight / $_unit / $viewBoxHeight"
            }
                #
            set scale           [lindex [lsort [list $sx $sy]] 1]
                #
                # puts "         \$sx     $sx"      
                # puts "         \$sy     $sy"      
                # puts "         \$scale  $scale"      
                #
            set tx 0
            set ty 0
                #
            if {$sx < $scale} {
                set ty  [expr {0.5 * ($viewBoxHeight - $viewHeight)}]
            }
            if {$sy < $scale} {
                set tx  [expr {0.5 * ($viewBoxWidth - $viewWidth)}]
            }
                #
                # set styleScale $scale    
                #
                # set ty    [expr {0.5 * ($viewBoxHeight - $viewHeight)}]
                # set tx    [expr {0.5 * ($viewBoxWidth - $viewWidth)}]
                #

                #    
            return  [list [list $scale 0 0 $scale $tx $ty] $scale]
                 #
        }
    }
        #
    method getViewBox {} {
            #
            #
            # set styleScale  1    
            #
        set viewWidth   0
        set viewHeight  0
            #
        if {[catch {set viewWidth   [$rootNode getAttribute width]}     eID]} {set viewWidth    {}}
        if {[catch {set viewHeight  [$rootNode getAttribute height]}    eID]} {set viewHeight   {}}
        if {[catch {set viewBox     [$rootNode getAttribute viewBox]}   eID]} {set viewBox      {}}
            #
            # puts "       -> \$viewWidth     $viewWidth"
            # puts "       -> \$viewHeight    $viewHeight"
            # puts "       -> \$viewBox       $viewBox"
            #
        if {$viewBox eq {}} {
            return {}
        } else {
            lassign $viewBox    x1 y1 viewBoxWidth viewBoxHeight
                # puts "       -> \$viewBox       $viewBox"
                # puts "       -> \$viewBoxWidth  $viewBoxWidth"
                # puts "       -> \$viewBoxHeight $viewBoxHeight"
                #
            if {$viewWidth eq {} && $viewHeight eq {}} {
                set viewWidth   [expr {$viewBoxWidth - $x1}]
                set viewHeight  [expr {$viewBoxWidth - $y1}]
            }
                #
            if {$viewWidth ne {}} {
                set viewWidth       [regsub -all -- {[a-zA-Z]} $viewWidth ""]
                set viewBoxWidth    [expr {$styleScale * $viewBoxWidth}]
            }
                # 
            if {$viewHeight ne {}} {
                set viewHeight      [regsub -all -- {[a-zA-Z]} $viewHeight ""]
                set viewBoxHeight   [expr {$styleScale * $viewBoxHeight}]
            }
                #
            if {$viewBoxWidth eq {} || $viewBoxHeight eq {}} {
                return {}
            }
                #
            return  [list 0 0 $viewBoxWidth $viewBoxHeight]
                 #
        }
    }
        #
    method getStyleScale {} {
        return $styleScale
    }
        #
    method getSVG {} {
            #
            # ----- this is the central Procedure -----
            #
            # puts "\n ===== getExportXML =========\n"
            #
        my GetBase
            #
        set parentNode  [$expDoc documentElement]
            #
            # my getNewSVG_02
            #
        puts "\n SVG -> getSVG\n"
            #
        puts "\n ----- input ---------------------------------------------"
        puts [$expDoc asXML]
        puts " ----- input ---------------------------------------------\n"
            #
            # ---- style ----
            #
        puts "\n ----- input --- UpdateRootStyle -------------------------"
            #
        my UpdateRootStyle
            #
        puts [$expDoc asXML]
        puts " ----- input ---------------------------------------------\n"
            #
            # ---- comments ----
            #
        puts "\n ----- input --- CleanupComments -------------------------"
            #
        my CleanupComments
            #
        puts [$expDoc asXML]
        puts " ----- input ---------------------------------------------\n"
            #
            # ---- content ---- parsing children that are SVGElements
            #
        foreach child $children {
            set childNode [$child createSVGNode $parentNode]
        }
            #
            #
            # puts "\n ----- flatten <g> -----\n"
            #
        if {$flattenMode eq {flat}} {
            my FlattenSingleGroup $parentNode
        }         
            #
            # 
        puts "\n ----- output --------------------------------------------"
        puts [$expDoc asXML]
        puts " ----- output --------------------------------------------\n"
            #
            #
        return $expDoc
            #
    }    
        #
    method UpdateRootStyle {} {
            #
        if {$styleNode eq {}} {
            return
        }
            #
            # puts "    --> \$styleNode $styleNode   <- [$styleNode parentNode]"
            # puts "[$styleNode asXML]"
            #
        set dictStyle   [$styleNode text]   
            #
            # puts "    --> \$dictStyle  $dictStyle"
            # puts "[dict print $dictStyle]"
            #
        dict for {key keyValue} $dictStyle {
                #
                # puts "\n----------------------------------------------"
                # puts "--> $key"
                # puts "        --> $keyValue"
                #
            set _style      [string map {\; { } : { }} $keyValue]
                #
                # puts "[dict print $_style] \n"
                #
            set _ObjStyle   [svgDOM::SVGStyle new $styleScale]
            $_ObjStyle      update $_style
            set resultDict  [$_ObjStyle get]
            $_ObjStyle      destroy
                #
            set newValue    {}
            foreach {_key _value} $resultDict {
                append newValue ${_key}:${_value}\;
            }
                #
            set newValue    [string trim $newValue \;]
                # puts "        --> $newValue"
            if {[string index $newValue 0] != "\}"} {
                set newValue "\{$newValue\}"
            }
                #
            dict set dictStyle $key $newValue
                #
        }
            #
            # puts "[dict print $dictStyle]"
            #
        set textStyle   "\n"
            #
            # puts "    xPath -> [llength [string map {/ { }} [$styleNode toXPath]]]"
        set repeatCount  [llength [string map {/ { }} [$styleNode toXPath]]]
        set repeatCount  [expr {4 * $repeatCount}]
            #
        dict for {key keyValue} $dictStyle {
                # puts "      -> $key $keyValue"
            append textStyle "[string repeat { } $repeatCount]${key} $keyValue \n"
                #
        }
        append textStyle [string repeat { } [expr {$repeatCount - 4}]]
        
            #
            # puts "$textStyle"  
            # puts "[$styleNode asXML]"    
            #
        foreach child [$styleNode childNodes] {
            $styleNode removeChild $child
            $child delete
        }
            #
        set nodeStyle [$doc createTextNode "$textStyle"]
            #
        $styleNode appendChild $nodeStyle
            #
            # puts "[$styleNode asXML]"    
            #
    } 
       
        #
    method CleanupComments {} {
            #
            # puts [$expDoc asXML]
            #
        foreach child [$expDoc childNodes] {
                # puts "    --> $child  <- [$child nodeName]  <- [$child nodeType]"
            if {[$child nodeType] eq {COMMENT_NODE}} {
                    # puts "    --> $child  <- [$child nodeName]  <- [$child nodeType]"
                $expDoc removeChild $child
                $child delete
            }
        }
            #
    }
        #
    method FlattenSingleGroup {parentNode} {
            #
            # puts "        -> FlattenSingleGroup [info level]"
            # puts "            -> $parentNode"
            # puts "                -> children: [$parentNode childNodes]"
            #
        foreach node [$parentNode childNodes] {
            switch -exact -- [$node nodeName] {
                g -
                svg {
                    if {[info level] > 2} {
                        # puts "[$node asXML]"
                    }
                    set childNodes  [$node childNodes]
                    if {[llength $childNodes] == 1} {
                        set childNode $childNodes
                            # puts "\n                   -> upstore [$childNode toXPath] -> [$childNode nodeName] -> [$childNode getAttribute id]"
                            # puts "                         -> \$parentNode  [$parentNode nodeName]"
                            # puts "                         -> \$node        [$node nodeName] -> [$node childNodes]"
                            # puts "                         -> \$childNode   [$childNode nodeName]"
                        $node removeChild $childNode
                        $parentNode replaceChild $childNode $node
                        $node delete
                    } elseif {[llength $childNodes] == 0 && [$node nodeName] eq {g}} {
                        $parentNode removeChild $node
                        $node delete
                    }
                    foreach childNode [[$parentNode childNodesLive] childNodes] {
                        my FlattenSingleGroup $childNode
                    }
                }
                default {
                    # puts "                   -> .. keep $node -> [$node asXML]"
                    # puts "                   -> .. keep [$node toXPath] -> [$node nodeName] -> [$node getAttribute id]"
                }
            }
        }
            #
        return
            #
    }
        #
    method GetBase {} {
            #
        set expDoc      [dom parse [$doc asXML]]
            #
        set mySVGNode   [$expDoc documentElement]     
            #
        set styleNode   [$mySVGNode getElementsByTagName style]
            #
            # puts "   -> getCleanDoc [$mySVGNode nodeName]"
            #
        set viewBox     [my getViewBox]
            #
            # puts "  -> \$viewBox $viewBox"
            #
        if {$viewBox ne {}} {
            $mySVGNode setAttribute viewBox $viewBox
        } else {
            if {[$mySVGNode hasAttribute viewBox]} {
                $mySVGNode removeAttribute viewBox
            }
        }
            #
        foreach child [$mySVGNode childNodes] {
            set nodeType    [$child nodeName]
                #puts "   -> getCleanDoc $nodeType"
            switch -exact -- $nodeType {
                g -
                rect -
                circle -
                ellipse -
                line -
                polyline -
                polygon -
                path {
                    # puts "  --- cleaning: $nodeType"
                    $mySVGNode removeChild $child
                    $child delete
                }
                default {}
            }
        }
            #
            # puts "-------------------------------------"
            # puts [$mySVGNode asXML]
            # puts "-------------------------------------"
            #
            # puts     
            #
        return $expDoc    
            #
    }    
        #
}

