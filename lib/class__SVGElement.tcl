#
# svgDOM
#
# tclSVG -- an oo hierarchy to parse, mutate and generate SVG
#
#   2018.05.30
#
#       https://wiki.tcl.tk/25998
#       https://wiki.tcl.tk/3650?redir=3654  -> Colin McCormack
#
#

    #
oo::define svgDOM::SVGElement {
        #
    variable  root parent node
    variable  id root node nodeName
    variable  children 
    variable  matrix shape style attrList 
    variable  shapeDict styleDict   ;# the new definition of the node
    variable  styleScale
    variable  toRad 
        #
        # variable  curveResolution
        #
    superclass svgDOM::Element      ;# the document is itself an element
        #
    constructor {_root _parent _node} {
            #
        set root        $_root    
        set parent      $_parent    
        set node        $_node
            #
        set toRad       [expr {4*atan(1)/180}]
            #
        set shapeDict   {}    
            #
        next $root $parent $node
            #
        set styleScale [$root getStyleScale]
            #
            # exit
            #
    }
        #
    destructor {
            #
        next
            #
    }
        #
    method unknown {target_method args} {
        puts "<E> ... svgDOM::Element $target_method $args  ... unknown"
    }    
        #
    method update {} {
            #
        next    ;# use method of superclass svgDOM::Element
            #
            #
            #
            # --- Style ---------------------
            #
        set style           [svgDOM::SVGStyle new [$root getStyleScale]]
            #
            # puts "   -> [$parent report]"
            # puts "   -> \$parent        $parent"
            # puts "   -> \$parent style  [$parent style]"
            #
        set parentStyle     [[$parent style] get]
            #
            # puts "   -> \$parentStyle $parentStyle"
            #
        $style update       $parentStyle
            #
        $style update       $attrList
            #
            #
            #
            # --- transform -> Matrix ------
            #
        set matrix          [svgDOM::SVGMatrix new]
            #
        set parentMatrix    [[$parent matrix] get]
            #
            # puts "       -> 001: $parentMatrix"
            #
        $matrix multiply    $parentMatrix   prepend
            #
            # puts "       -> 002: [$matrix get]"
            #
            # puts "   ----> $attrList"
        if {[dict exists $attrList transform]} {
                puts "   -> [dict get $attrList transform]"
            set cmd         [my TransformAttrToList [dict get $attrList transform]]
                puts "       -> 003: $cmd"
            $matrix transform   $cmd
                #
        }
            #
        # return
            #
            # puts "       -> 004: [$matrix get]"
            #
            #
            # --- Shape ---------------------
            #
        switch -exact -- $nodeName  {
            circle {    
                set shape       [svgDOM::Circle     new $attrList $matrix [$root getStyleScale]]
                set shapeDict   [$shape get]
                set styleDict   [$style get]
                # puts "                     ----> circle:    \$shapeDict"
                # puts "                     ----> ellipse:   \$styleDict -> $styleDict"
                catch {set styleDict [dict remove $styleDict transform]}
                # puts "                     ----> ellipse:   \$styleDict -> $styleDict"            }
            ellipse {   
                set shape       [svgDOM::Ellipse    new $attrList $matrix [$root getStyleScale]]
                set shapeDict   [$shape get]
                set styleDict   [$style get]
                # puts "                     ----> ellipse:   \$shapeDict"
                # puts "                     ----> ellipse:   \$shapeDict -> $shapeDict"
                # puts "                     ----> ellipse:   \$styleDict -> $styleDict"
                catch {set styleDict [dict remove $styleDict width]}
                catch {set styleDict [dict remove $styleDict height]}
                catch {set styleDict [dict remove $styleDict transform]}
                # puts "                     ----> ellipse:   \$styleDict -> $styleDict"
            }
            line {      
                set shape       [svgDOM::Line       new $attrList $matrix]
                set shapeDict   [$shape get]
                set styleDict   [$style get]
                # puts "                     ----> line:      \$shapeDict"
            }
            path  {     
                set shape       [svgDOM::Path       new $attrList $matrix]
                set shapeDict   [$shape get]
                set styleDict   [$style get]
                # puts "                     ----> path:      \$shapeDict"
            }
            polygon {   
                set shape       [svgDOM::Polygon    new $attrList $matrix]
                set shapeDict   [$shape get]
                set styleDict   [$style get]
                # puts "                     ----> polygon:   \$shapeDict"
            }
            polyline {  
                set shape       [svgDOM::Polyline   new $attrList $matrix]
                set shapeDict   [$shape get]
                set styleDict   [$style get]
                # puts "                     ----> polyline:  \$shapeDict"
            }
            rect {      
                set shape       [svgDOM::Rectangle  new $attrList $matrix]
                set shapeDict   [$shape get]
                set styleDict   [$style get]
                # puts "                     ----> rect:      [$matrix get]"
                # puts "                     ----> rect:      \$shapeDict -> $shapeDict"
                # puts "                     ----> rect:      \$styleDict -> $styleDict"
                catch {set styleDict [dict remove $styleDict width]}
                catch {set styleDict [dict remove $styleDict height]}
                catch {set styleDict [dict remove $styleDict transform]}
                # puts "                     ----> rect:      \$styleDict -> $styleDict"
            }            
            g {      
                set styleDict   [$style get]
                # puts "                     ----> group:     [$matrix get]"
            }            
            __any__ {
                set shape       {}
                    # set shape   [svgDOM::SVGShape new $nodeName $attrList]
            }
        }
            #
            #
        return
            #
    }    
        #
    method createElement {_doc _name} {
        set node        [$_doc createElement $_name]
        return $node
    }
    method createElementNS {_doc _name} {
        set svgNS       "http://www.w3.org/2000/svg" 
        set node        [$_doc createElementNS $svgNS $_name]
        return $node
    }
        #
        #
        # -- create node from element object --
        #
    method createSVGNode {parentNode} {
            #
        set doc         [$root getExpDoc]
            #
            # puts "  ---- createSVGNode: ----"
            # puts "      \$nodeName $nodeName"
            #
        switch -exact -- $nodeName  {
            rect -
            circle -
            ellipse -
            line -
            polyline -
            polygon {
                    #
                if {$shapeDict ne {}} {
                        #
                        # -- inherited from SVGShape (rect, circle , ellipse, line, polyline, polygon, path)
                        #
                        puts "    -> createSVGNode -> $doc  \$shapeDict $shapeDict"
                        puts "    -> createSVGNode -> $doc  \$styleDict $styleDict"
                        #
                    lassign $shapeDict  nodeType shapeDefinition
                        # puts "           -> $nodeName  -> $nodeType - $shapeDefinition"
                    set exportNode  [my createElementNS $doc $nodeType]
                        #
                    set nodeID  [$root unique_id $id]
                    $exportNode setAttribute    id $nodeID
                        #
                    dict for {key value} [dict merge $styleDict $shapeDefinition] {
                        $exportNode setAttribute $key $value
                        puts "      -> $key $value"
                    }
                        #
                    $parentNode appendChild $exportNode
                        #
                } else {
                        #
                    puts "   --> exception 01: $nodeName"
                    set exportNode  [my createElement $doc $nodeName]
                        #
                }
            }
            path {
                    #
                    # puts "  ... [$root pathType]"
                    #
                $shape type [$root pathType]
                    #
                set shapeDict   [$shape get] 
                    #
                    # puts "\n"
                    # puts "  ---- path: ----"
                    # puts "     \$shapeDict:"
                    # puts "[dict print $shapeDict]"
                    # puts "\n"
                    #
                    #     {
                    #         id _uid_3_0
                    #         g {
                    #             0 {
                    #                 path {
                    #                     d {M 45.74,113.56 66.23,113.56 66.23,124.13000000000001 45.74,113.56 45.74,113.56 Z M 66.23,144.95000000000002 66.23,163.46 56.68,163.46 66.23,144.95000000000002 Z}
                    #                 }
                    #             }
                    #             1 {
                    #                 polygon {
                    #                     points {}
                    #                 }
                    #             }
                    #             2 {
                    #                 polyline {
                    #                     points {}
                    #                 }
                    #             }
                    #        }
                    #     }
                    #
                if {$shapeDict eq {}} {
                        #
                    puts "   --> exception 01: $nodeName"
                        # set exportNode  [my createElement $doc $nodeName]
                        #
                } else {
                        #
                    if 0 {
                        set pathDict    [dict getnull $shapeDict path]
                        if {$pathDict eq {}} {
                            continue
                        } else {
                            set shapeDict $pathDict
                        }
                    }
                        #
                    set resultType  [dict get $shapeDict type]
                    set contentDict [dict get $shapeDict content]
                        #
                        # puts "     \$contentDict:"
                        # puts "[dict print $contentDict]"
                        #
                    set nodeID      [dict get $contentDict id]
                    set nodeID      [$root unique_id $nodeID]
                        #
                        # puts "--- \$nodeName $nodeName -- $resultType"
                        #
                    switch -exact -- $resultType {
                        group {
                                #
                                # puts "--- \$nodeName $nodeName -- $resultType <-- group"
                                #
                            set dictGroup   [dict get $contentDict g]
                                #
                                # puts "      ... complex Path [dict get $shapeDict id]"    
                                #
                            set exportNode  [my createElementNS $doc g]
                                #
                            $exportNode setAttribute id $nodeID
                                #
                            $parentNode appendChild $exportNode
                                #
                            set parentNode $exportNode
                                #
                            dict for {_id _dict} $dictGroup {
                                    #
                                    # puts "              -> $id   ... $_dict"
                                    #
                                lassign $_dict  nodeType shapeDefinition
                                    # puts "           -> $nodeName  -> $nodeType - $shapeDefinition"
                                set childNode  [my createElementNS $doc $nodeType]
                                dict for {key value} [dict merge $styleDict $shapeDefinition] {
                                    $childNode setAttribute $key $value
                                    # puts "        ->  $key / $value"
                                }
                                    #
                                $childNode  setAttribute    id [format {%s_%s} $id $_id]
                                    #
                                $parentNode appendChild     $childNode
                                    #
                            }
                        }
                        path {
                                #
                                # puts "--- \$nodeName $nodeName -- $resultType <-- path"
                                #
                            set exportNode      [my createElementNS $doc $resultType]
                                #
                            $exportNode setAttribute id $nodeID
                                #
                                # puts "  ------ \$exportNode [$exportNode asXML]"
                                #
                            dict for {key value} [dict merge $styleDict] {
                                $exportNode setAttribute $key $value
                                # puts "        ->  $key / $value"
                            }
                                #
                            set pathDescription [dict get $contentDict d]
                            if {$pathDescription ne {}} {
                                    #
                                $exportNode setAttribute     d      [dict get $contentDict d]
                                    #
                                    #
                                    # puts "  ------ \$exportNode [$exportNode asXML]"
                                    #
                                    # puts "[dict print $contentDict]"
                                    #
                                # $exportNode setAttribute    d   [dict get $contentDict d]
                                    #
                                $parentNode appendChild     $exportNode 
                                    #
                            }
                        }
                        polygon -
                        polyline {
                                #
                                # puts "--- \$nodeName $nodeName -- $resultType <-- polygon / polyline"
                                #
                            set exportNode      [my createElementNS $doc $resultType]
                                #
                            $exportNode setAttribute    id $nodeID
                                #
                            dict for {key value} [dict merge $styleDict] {
                                $exportNode setAttribute $key $value
                                # puts "        ->  $key / $value"
                            }
                                #
                            $exportNode setAttribute    points  [dict get $contentDict points]
                                #
                            $parentNode appendChild     $exportNode
                                #
                        }
                        default {
                            puts "\n        <W> [self] -> createSVGNode --> $resultType from path-object not recognized!\n"
                            return {}
                        }
                    }
                }
            }
            g {
                        #
                        # puts "   -----------  "
                        # puts "   -> g: $attrList    "
                        # puts "    -> createSVGNode -> $doc g $attrList"
                        #
                    set exportNode  [my createElementNS $doc g]
                        #
                    set nodeID      [$root unique_id $id]
                    $exportNode setAttribute    id $nodeID
                        #
                    $parentNode appendChild     $exportNode
                        #
                        # puts "   -> [$exportNode attributes]"
                        # puts "   -> [$exportNode attributes]"
                        # puts "    -> createSVGNode -> $nodeName - $id <- [self]  "
                        # puts "---- DEBUG ---- g ---- init ----"
                        # puts "[$exportNode asXML]"
                        #
                    foreach child $children {
                        set childNode [$child createSVGNode $exportNode]
                        # $exportNode appendChild $childNode 
                    }
                        #
                        # puts "[$exportNode asXML]"
                        # puts "---- DEBUG ---- g ---- end ----\n\n"
                        #
            }
            default {
                    puts "   --> exception 02: $nodeName"
                    puts ""
                    # set exportNode  [my createElement $doc $nodeName]
            }
        }
            #
        # return $exportNode
            #
    }
        #
}
