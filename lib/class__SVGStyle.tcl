 #
 # svgDOM
 #
 #   2018.06.06 
 #
 #

oo::define svgDOM::SVGStyle {
        #
    variable styleDict
    variable styleScale
        #
    constructor {{_styleScale 1}} {
            #
        set styleScale $_styleScale  
        set styleDict       {}   
            #
    }
    destructor { 
        puts "            -> [self] ... destroy SVGStyle"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::SVGStyle::$target_method $args  ... unknown"
    }
        #
    method update {_dict} {
            #
            # puts "                -> update: $styleScale"
            # puts "                -> update: $_dict"
            #
        dict for {key value} $_dict {
                # puts "            -> $key - $value"
            switch -exact -- $key {
                class -
                fill -
                stroke {
                    dict set styleDict $key $value
                }
                stroke-width {
                        # puts "            -> $key - $value"
                    set width   [regsub -all -- {[a-zA-Z]} $value ""]
                    dict set styleDict $key [expr {$width * $styleScale}]
                }
                style {
                        # {fill:#c2c1c1;stroke:#da251d;stroke-width:0.17640001}
                        # puts "   style -> $value"
                    set __dict  [string map {: { }  \; { }} $value]
                        # puts "   style -> $__dict"
                    my update $__dict
                        #
                }
                default {
                    dict set styleDict $key $value
                }
            }
        }
            #
            # puts "                      ---> $styleDict"
            # exit
            #
    }    
        #
    method get {} {
        return $styleDict
    }    
        #
}
