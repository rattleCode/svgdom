 #
 # svgDOM
 #
 # https://www.w3.org/TR/SVG/shapes.html
 #
 #  2018.05.30 
 #
 #

oo::define svgDOM::PathFraction {
        #
        # https://www.w3.org/TR/SVG/shapes.html
        #
        # rect 
        # circle 
        # ellipse 
        # line 
        # polyline 
        # polygon         
        #
    variable attrList matrixObj
    variable polygon polyline
    variable shapeDict                  ;# the returning node definition
        #
    variable pathDefinition
    variable pathDef_Normalized
    variable pathDef_Absolute
    variable pathDef_Splited
        #
    variable svgPrecision
        #
    variable _shapeDict
        #
    superclass svgDOM::SVGPath          ;# the template class of every element
        #
    constructor {_attrList _matrixObj} {
            #
            # puts "\n<I>\n<I> ... svgDOM::PathFraction \n<I>"
            #
        set svgPrecision    1    
            #
        set attrList    $_attrList
        set matrixObj   $_matrixObj
            #
            # puts "            -> Path:      $attrList"
            # puts "                          $matrix"
            #
            #
        set pointList   {}
        set _shapeDict  {}
        set shapeDict   {}
        
            #
        next            path    $attrList $_matrixObj
            #
        set pathDefinition      [dict get $attrList d]
            #
        set pathDef_Normalized  [my NormalizePath   $pathDefinition]
            #
        # puts "                  -> polygon:   -> $polygon"    
        # puts "                  -> polyline:  -> $polyline"    
            #
    }
        #
    destructor { 
        puts "            -> [self] ... destroy svgDOM::PathFraction"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::PathFraction::$target_method $args  ... unknown"
    }
        #
    method get {} {
            #
        puts "\n === PathFraction -> get ===\n"
            #
        set pathDef_Absolute    [my AbsolutePath    $pathDef_Normalized]
            #
        puts "        -> \$pathDef_Absolute: $pathDef_Absolute"
            #
        set pathDef_Splited     [my SplitPath       $pathDef_Absolute]
            #
        puts "        -> \$pathDef_Splited:  $pathDef_Splited"
            #
        set shapeDict           [my ToShapeDict     $pathDef_Splited]
            #
        puts "        -> \$shapeDict:" 
        puts "[dict print $shapeDict]"
            #
        return $shapeDict
            #
    }
        #
    method SplitPath {pathDef} {
            #
        set shapeDict   {}  ; # will become shapeDict in future ...
            #
            #
            # puts "\n == SplitPath ===========================\n\n  -> $pathDef"
            #
            #
        if {[dict exist $attrList id]} {
            set nodeID [dict get $attrList id]
        } else {
            set nodeID [clock microseconds]
        }
            #        
        dict set shapeDict id $nodeID
        dict set shapeDict g  {}
            #
            # puts "  -> \$__shapeDict   ->  $__shapeDict"
            # puts "  -> \$pathDef   ->  $pathDef"
            #
        set indexList       [lsearch -exact -all $pathDef M]
        lappend indexList   [llength $pathDef]    ;# $i is not an index in the list but a limiter
            # puts "  -> \$indexList   ->  $indexList"
            # puts "       -> [lindex $pathDef [llength $pathDef]-2]"
            # puts "       -> [lindex $pathDef [llength $pathDef]-1]"
            #
        set segmentList   {}    
            # set __splitList {}
            #
        set i 0
            #
        foreach j [lrange $indexList 1 end] {
                # puts "      b $i -> $j"
                # continue
            set pathSegment [lrange $pathDef $i $j-1]
                # puts "    -> \$pathSegment $pathSegment"
            lappend segmentList $pathSegment
            set i $j
        }
            #
            
            
        set i 0
        foreach pathSegment $segmentList {
                #
                #puts "  -> \$pathSegment  $pathSegment"
                #
            set closeNode   [lindex $pathSegment end-1]
                #
                #puts "  -> \$closeNode    $closeNode"
                #
            set pointList   [my CreatePointList  $pathSegment]
                #
                # puts "        05 -> \$pointList \n$pointList"
                #
            set valueCount  [llength $pointList]
            if {$valueCount <= 1} {
                set pointList {} 
            }
                #
            if {$pointList != {}} {
                    #
                if {$closeNode == {Z} } {
                    set segmentDict [dict create polygon  [dict create points $pointList]]
                } else {
                    set segmentDict [dict create polyline [dict create points $pointList]]
                }
                    #
                set loopID [format "%s___%s" $nodeID $i]
                    # puts "  --> \$loopID $loopID"
                    # dict set loopNode id $loopID
                    #
                    #
                dict set shapeDict g $i $segmentDict
                    #
            }
            incr i
                #
        }
            #
            # puts "-----> \$shapeDict \n$shapeDict\n<---"    
            #
        return $shapeDict
            #
    }
        #
    method checkPoly__x {node} {
            #
        # variable trashNode
            #
        # puts ""
        puts "checkPoly__x:   -> $node"
        # puts ""
            #
            # points="52.43729999999999,-32.5785"
            #
            # set nodeName        [$node nodeName]
        set nodeName        [lindex $node 0]
        set nodeAttr        [lindex $node 1]
            # puts "   -> \$nodeName          $nodeName"
            #
        switch -exact $nodeName {
            polygon  -
            polyline {
                        # set pointAttribute  [$node getAttribute points]
                    set pointAttribute  [dict get $nodeAttr points]
                        # puts "   -> \$pointAttribute    $pointAttribute"
                    set pointCount      [llength $pointAttribute]
                        # puts "   -> \$pointCount        $pointCount"
                        #
                    if {$pointCount > 1} {
                        # puts "   -> \$node $node"
                        return $node
                    } else {
                        # puts "   -> \$trashNode $trashNode"
                        # puts "   -> \$node $node"
                        # $trashNode appendChild $node
                        return {} 
                    }
                }
            default {
                return $node
            }
        }
            #
    }
        #
    method ToShapeDict {_shapeDict} {
            #
        dict set shpDict  type group 
        dict set shpDict  content id [dict get $_shapeDict id] 
            #
        dict for {_id _dict} [dict get $_shapeDict g] {
                #
            dict for {__id __dict} [dict get $_shapeDict g] {
                puts "    -> $_id $_dict "
                dict set shpDict content g $__id $__dict
            }
        }  
            #
        return $shpDict
            #
    }
        #
}        