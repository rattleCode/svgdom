 #
 # svgDOM
 #
 # https://www.w3.org/TR/SVG/shapes.html
 #
 #  2018.05.30 
 #
 #

oo::define svgDOM::SVGShape {
        #
        # https://www.w3.org/TR/SVG/shapes.html
        #
        # rect 
        # circle 
        # ellipse 
        # line 
        # polyline 
        # polygon         
        #
    variable type attrList matrixObj   
    variable polygon polyline matrixObj
    variable shapeDict                   ;# the returning node definition
        #
    constructor {_type _attrList _matrixObj} {
            #
        set type        $_type
        set attrList    $_attrList
        set matrixObj   $_matrixObj
            #
        # puts "            -> SVGShape:   $type -> $attrList"   
            #
    }
        #
    destructor { 
        puts "            -> [self] ... destroy SVGShape"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::SVGShape::$target_method $args  ... unknown"
    }
        #
    method transform {pointList} {
            #
            # pointList:   {x y  x y  x y  x y  ...}
            #
            #  -- the art of handling the transform matrix in SVG
            #           got it from here: http://commons.oreilly.com/wiki/index.php/SVG_Essentials/Matrix_Algebra
            #       | a  c  tx |                    | a*x  c*y  tx*1 |  -> x
            #       | b  d  ty |  *  |x  y  1|  =   | b*x  d*y  ty*1 |  -> y
            #       | 0  0   1 |                    | 0*x  0*y   1*1 |  -> z (not interesting in a 2D)   
            #
            # puts "    transform_SVGObject: $matrix"
            #
            #
            # puts "        -> transform: $pointList"
            #
        set pointList_new   {}
            #
            # puts "       -> 090: [$matrixObj get]"
            #
        lassign [$matrixObj get]    a b c d tx ty
            #
        foreach {x y} $pointList {
                #
            set x [expr {1.0 * $x}]
            set y [expr {1.0 * $y}]
                # puts "\n--------------------"
                # puts "       -> $x $y"
            set new_x [ expr {$a*$x + $c*$y + $tx}]
            set new_y [ expr {$b*$x + $d*$y + $ty}]
                # puts "          x/y:  $x / $y"
                # puts "          a/b -> xt:  $a / $b -> $new_x"
                # puts "          c/d -> yt:  $c / $d -> $new_y"
            lappend pointList_new   $new_x $new_y
                #
        }
            #
            # puts "       -> 099: $pointList_new"    
            #
        return $pointList_new
            #
    }
        #
    method get {} {
        return $shapeDict
    }
        #
 
}        