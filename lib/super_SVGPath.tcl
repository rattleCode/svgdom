 #
 # svgDOM
 #
 # https://www.w3.org/TR/SVG/shapes.html
 #
 #  2018.05.30 
 #
 #

oo::define svgDOM::SVGPath {
        #
        # https://www.w3.org/TR/SVG/shapes.html
        #
        # rect 
        # circle 
        # ellipse 
        # line 
        # polyline 
        # polygon         
        #
    variable attrList matrixObj
    variable polygon polyline
    variable shapeDict                  ;# the returning node definition
        #
    variable pathDefinition
    variable pathDef_Normalized
    variable pathDef_Absolute
    variable pathDef_Splited
        #
    variable svgPrecision
        #
    variable _shapeDict
        #
    superclass svgDOM::SVGShape         ;# the template class of every element
        #
    constructor {_attrList _matrixObj} {
            #
        set svgPrecision    1    
            #
        set attrList    $_attrList
        set matrixObj   $_matrixObj
            #
            # puts "            -> Path:      $attrList"
            # puts "                          $matrix"
            #
            #
        set pointList   {}
        set _shapeDict  {}
        set shapeDict   {}
        
            #
        next            path    $attrList $_matrixObj
            #
        set pathDefinition      [dict get $attrList d]
            #
        set pathDef_Normalized  [my NormalizePath   $pathDefinition]
            #
        # puts "                  -> polygon:   -> $polygon"    
        # puts "                  -> polyline:  -> $polyline"    
            #
    }
        #
    destructor { 
        puts "            -> [self] ... destroy svgDOM::SVGPath"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::SVGPath::$target_method $args  ... unknown"
    }
        #
    method type {{mode fraction}} {
            #
        switch -exact $mode {
            fraction {
                oo::objdefine [self] class svgDOM::PathFraction
            }
            default {
                oo::objdefine [self] class svgDOM::Path
            }
        }
        
            #
    }
        #
    method get {} {
            #
        set pathDef_Absolute    [my AbsolutePath    $pathDef_Normalized]
            #
        set shapeDict           [my ToShapeDict     $pathDef_Absolute]
            #
            # puts $shapeDict
            #
        return $shapeDict
            #
    }
        #        
    method NormalizePath {pathDef} {
            #
            # puts "\n== Path2Dict == 01 ==\n"    
            #
            # puts "\n ---------"
            # puts "$pathDefinition"
            #
        set pathDefinition_00   [string map { M { M }   Z { Z }   L { L }   H { H }   V { V }   C { C }   S { S }   Q { Q }   T { T }   A { A }  \
                                              m { m }   z { Z }   l { l }   h { h }   v { v }   c { c }   s { s }   q { q }   t { t }   a { a }  , { }  } \
                                    [string trim $pathDef]]
            #
            # puts "->\$pathDefinition_00 \n$pathDefinition_00"
            # puts "\n ---------"
            # 001:  set pathDefinition   [regsub -all {\y-} $pathDefinition { -}]
            # puts "\n"
            # puts "------------------------------------------"
            # puts "->\$pathDefinition \n$pathDefinition"
        set pathDefinition_01   [regsub -all {\y-} $pathDefinition_00 { -}]
        set pathDefinition_01   [string map {{e -} {e-}} $pathDefinition_01]
            # puts "------------------------------------------"
            # puts "->\$pathDefinition_01 \n$pathDefinition_01"
            #
            #
            #
            # -- compute math values, e.g: 2.56e-6
            #
        set pathDefinition_02   {}
            #
        foreach value $pathDefinition_01 {
              # puts "       -> $xy"
            if {[string first {10e} $value] >= 0} {
                    # ... sometimes the pathDefinition contains values like: 10e-4
                    # these values does not work in tcl
                    # therfore
                    # puts "           -> check:  $xy"
                set exponent [lindex [split $value e] 1]
                set value [expr {1.0 * pow(10,$exponent)}]
            }
            lappend pathDefinition_02 $value
        }
            #
            # puts "->\$pathDefinition_02 \n$pathDefinition_02"
            #
            #
            #
            # -- create path-dictionary
            #
        set indexList   {}
        set i           0
        foreach element $pathDefinition_02 {
              # puts "  -> $element"
            if {[string match {[A-Z]} $element]} {
               lappend indexList $i
            }
            if {[string match {[a-z]} $element]} {
               lappend indexList $i
            }
            incr i
        }
            #
        lappend indexList $i    ;# $i is not an index in the list but a limiter
            #
            # puts "\n    a -> $indexList"    
            #
        set pathDefinition_03   {}
        set i 0
        foreach j [lrange $indexList 1 end] {
                # puts "      b $i -> $j"
            set key     [lindex $pathDefinition_02 $i]
            if {$key eq "Z" && $key eq "z"} {
                lappend pathDefinition_03 $key {}
                continue
            } else {
                set value   [lrange $pathDefinition_02 $i+1 $j-1]
                # puts "        c -> $value"
                lappend pathDefinition_03 $key $value
                set i $j
                # puts "          d -> $i"
            }
        }
            #
            # puts "->\$pathDefinition_03 \n$pathDefinition_03"
            #
            # puts "\n   [llength $$pathDefinition_02] == [lindex $indexList end] \n"
            #
        if {([llength $pathDefinition_02] - 1) == [lindex $indexList end]} {
            # puts "\n   [llength $$pathDefinition_02] == [lindex $indexList end] \n"
            #set key     [lindex $pathDefinition_02 [lindex $indexList end]]
            #lappend pathDefinition_03 $key {}
        }
            #
            # puts "->\$pathDefinition_03 \n$pathDefinition_03"
            #

        if 0 {
            foreach {key value} $pathDefinition_03 {
                puts "        a -> $key <- $value"
            }
        }
            #
            # puts "\n== Path2Dict == 02 ==\n"    
            #
            #
        return $pathDefinition_03
            #
    }    
        #
    method AbsolutePath {pathDef} {
            #
            # https://www.w3.org/TR/SVG/paths.html#InterfaceSVGPathSeg
            #
        array \
            set penPosition { x 0 \
                              y 0 }
                                 
        
            # -- loop throug pathDefinition
            #
        set pathList        {}
            #
        foreach {control valueList} $pathDef {
                #
                # puts "        b -> $control <- $valueList"
                # continue
            set _valueList {}
                #
            switch -exact $control {
                M {     #
                        # --- PATHSEG_MOVETO_ABS 
                        #
                    foreach {x y} $valueList {
                        set penPosition(x)      $x    ; incr listIndex 
                        set penPosition(y)      $y    ; incr listIndex
                    }
                    lappend pathList M $valueList
                }
                m {     #
                        # --- PATHSEG_MOVETO_REL 
                        #
                    foreach {x y} $valueList {
                        set penPosition(x)      [expr {$x + $penPosition(x)}] 
                        set penPosition(y)      [expr {$y + $penPosition(y)}]
                        lappend _valueList      $penPosition(x) $penPosition(y)
                    }
                    lappend pathList M $_valueList
                }
                L {     #
                        # --- PATHSEG_LINETO_ABS 
                        #
                    foreach {x y} $valueList  {
                        set penPosition(x)      $x
                        set penPosition(y)      $y
                    }
                    lappend pathList L $valueList
                }                    
                l {     #
                        # --- PATHSEG_LINETO_REL
                        #
                    foreach {x y} $valueList {
                        set penPosition(x)      [expr {$x + $penPosition(x)}] 
                        set penPosition(y)      [expr {$y + $penPosition(y)}]
                        lappend _valueList      $penPosition(x) $penPosition(y)
                    }
                    lappend pathList L $_valueList
                }
                C {     #
                        # --- PATHSEG_CURVETO_CUBIC_ABS
                        #
                    foreach {x y} $valueList  {
                        set penPosition(x)      $x
                        set penPosition(y)      $y
                        lappend _valueList      $penPosition(x) $penPosition(y)
                    }
                    lappend pathList C $_valueList
                }                    
                c {     #
                        # --- PATHSEG_CURVETO_CUBIC_REL
                        #
                    set bezierIndex             0
                            #
                    foreach {x y} $valueList {
                            # puts "       -> \$x $x"
                            # puts "       -> \$y $y"
                        set ctrlPosition(x)     [expr {$x + $penPosition(x)}]
                        set ctrlPosition(y)     [expr {$y + $penPosition(y)}]
                            # puts "           -> \$ctrlPosition(x) $ctrlPosition(x)"
                            # puts "           -> \$ctrlPosition(y) $ctrlPosition(y)"
                        lappend _valueList      $ctrlPosition(x) $ctrlPosition(y)
                            # puts "       -> \$bezierIndex $bezierIndex"
                            # puts "     -> \$valueList $valueList"
                            # puts "     -> \$_valueList $_valueList"
                        if {$bezierIndex > 1} {
                            # ... the first 2 points just inits the bezier
                            # puts "           -> \$bezierIndex $bezierIndex"
                            set penPosition(x)  $ctrlPosition(x) 
                            set penPosition(y)  $ctrlPosition(y)
                            set bezierIndex 0
                        } else {
                            incr bezierIndex
                        }
                            # parray penPosition
                    }
                    lappend pathList C $_valueList
                }
                Q {     #
                        # --- PATHSEG_CURVETO_QUADRATIC_ABS
                        #
                    puts "    $control  ... not implemented yet ($valueList)"
                }
                q {     #
                        # --- PATHSEG_CURVETO_QUADRATIC_REL
                        #
                    puts "    $control  ... not implemented yet ($valueList)"
                }
                A {     #
                        # --- PATHSEG_ARC_ABS  
                    puts "    $control  ... not implemented yet ($valueList)"
                    lassign $valueList  rx ry x_axis_rotation large_arc_flag sweep_flag x y
                    puts "        \$_x              $rx"
                    puts "        \$ry              $ry"
                    puts "        \$x_axis_rotation $x_axis_rotation"
                    puts "        \$large_arc_flag  $large_arc_flag"
                    puts "        \$sweep_flag      $sweep_flag"
                    puts "        \$x               $x"
                    puts "        \$y               $y"
                    
                    # rx ry x-axis-rotation large-arc-flag sweep-flag x y
                    # https://gist.github.com/timo22345/9413158
                    # https://developer.mozilla.org/de/docs/Web/SVG/Tutorial/Pfade
                    
                }
                a {     #
                        # --- PATHSEG_ARC_REL
                        #
                    puts "    $control  ... not implemented yet ($valueList)"
                    lassign $valueList  r_x r_y rot_x short_long dir end_x end_y
                }
                H {     #
                        # --- PATHSEG_LINETO_HORIZONTAL_ABS 
                        #
                    set x                       $valueList
                    set penPosition(x)          $x
                    lappend _valueList          $penPosition(x) $penPosition(y)
                    lappend pathList            L $_valueList
                }
                h {     #
                        # --- PATHSEG_LINETO_HORIZONTAL_REL 
                        #
                    set x                       $valueList
                    set penPosition(x)          [expr {$x + $penPosition(x)}]
                    lappend _valueList          $penPosition(x) $penPosition(y)
                    lappend pathList            L $_valueList
                }
                V {     #
                        # --- PATHSEG_LINETO_VERTICAL_ABS 
                        #
                    set y                       $valueList
                    set penPosition(y)          $y
                    lappend _valueList          $penPosition(x) $penPosition(y)
                    lappend pathList            L $_valueList
                }
                v {     #
                        # --- PATHSEG_LINETO_VERTICAL_REL 
                        #
                    set y                       $valueList
                    set penPosition(y)          [expr {$y + $penPosition(y)}]
                    lappend _valueList          $penPosition(x) $penPosition(y)
                    lappend pathList            L $_valueList
                }
                S {     #
                        # --- PATHSEG_CURVETO_CUBIC_SMOOTH_ABS        
                        #
                    foreach {x y} $valueList  {
                        set penPosition(x)      $x
                        set penPosition(y)      $y
                        lappend _valueList      $penPosition(x) $penPosition(y)
                    }
                    lappend pathList S $_valueList
                }
                s {     #
                        # --- PATHSEG_CURVETO_CUBIC_SMOOTH_REL
                        #
                    set bezierIndex             0
                            #
                    foreach {x y} $valueList {
                            # puts "       -> \$x $x"
                            # puts "       -> \$y $y"
                        set ctrlPosition(x)     [expr {$x + $penPosition(x)}]
                        set ctrlPosition(y)     [expr {$y + $penPosition(y)}]
                            # puts "           -> \$ctrlPosition(x) $ctrlPosition(x)"
                            # puts "           -> \$ctrlPosition(y) $ctrlPosition(y)"
                        lappend _valueList      $ctrlPosition(x) $ctrlPosition(y)
                            # puts "       -> \$bezierIndex $bezierIndex"
                            # puts "     -> \$valueList $valueList"
                            # puts "     -> \$_valueList $_valueList"
                        if {$bezierIndex > 0} {
                            # ... the first 2 points just inits the bezier
                            # puts "           -> \$bezierIndex $bezierIndex"
                            set penPosition(x)  $ctrlPosition(x) 
                            set penPosition(y)  $ctrlPosition(y)
                            set bezierIndex 0
                        } else {
                            incr bezierIndex
                        }
                            # parray penPosition
                    }
                    lappend pathList S $_valueList
                }
                T {     #
                        # --- PATHSEG_CURVETO_QUADRATIC_SMOOTH_ABS
                        #
                    puts "    $control  ... not implemented yet"
                }
                t {     #
                        # --- PATHSEG_CURVETO_QUADRATIC_SMOOTH_REL
                        #
                    puts "    $control  ... not implemented yet"
                }
                Z -
                z {     #
                        # PATHSEG_CLOSEPATH
                        # 
                    lappend pathList            Z {}
                }

                default {
                            puts "    $control  ... not registered yet"
                }    
            }
        
        }      
            #
            # puts "  -> \$pathList $pathList"     
            #
        return $pathList    
            #
     }
        #
    method TransformMatrix {valueList matrix} {
        
            #
            #  -- the art of handling the transform matrix in SVG
            #           got it from here: http://commons.oreilly.com/wiki/index.php/SVG_Essentials/Matrix_Algebra
            #       | a  c  tx |                    | a*x  c*y  tx*1 |  -> x
            #       | b  d  ty |  *  |x  y  1|  =   | b*x  d*y  ty*1 |  -> y
            #       | 0  0   1 |                    | 0*x  0*y   1*1 |  -> z (not interesting in a 2D)   
            #
            # puts "    transform_SVGObject: $matrix"
        
            # puts "    TransformMatrix: $valueList"
            # puts "    TransformMatrix: [llength $valueList]"
            # puts "    TransformMatrix: $matrix"
        
        variable svgPrecision
        
        set valueList_Return {}
            
        foreach {a b c d e f} $matrix break

        foreach {x y} $valueList {
                #
                # puts "      -> \$x $x"    
                # puts "      -> \$y $y" 
            if {$y eq {}} continue
                # if {$y eq {}} {set y 0}
                #
            set x [expr {1.0 * $x}]
            set y [expr {1.0 * $y}]
                # puts "\n--------------------"
                # puts "       -> $x $y"
            set new_x [ expr {$a*$x + $c*$y + $e}]
            set new_y [ expr {$b*$x + $d*$y + $f}]
                # puts "          x/y:  $x / $y"
                # puts "          a/b -> xt:  $a / $b -> $new_x"
                # puts "          c/d -> yt:  $c / $d -> $new_y"
                #
            set valueList_Return [lappend valueList_Return $new_x $new_y]
                #
        }
            #
        return $valueList_Return
            #
    }
        #
    method CreatePointList {pathDef} {
            #
            # puts "     -> CreatePointList \$pathDef $pathDef"
            #
        set pointList [my Path2Line $pathDef]
            # puts "\n"
            # puts "     -> CreatePointList \$pointList $pointList"
            # exit
            # -- handle transformation by given attribute
        set pointList [my TransformMatrix $pointList [$matrixObj get]]
            # puts "   -> $pointList"
            
            # -- format list
        set exportList {}
        foreach {x y} $pointList {
            lappend exportList "$x,$y"
        }  
            # puts "   -> $exportList"
        
            #
        return $exportList
            #
    }
        #
    method Path2Line {pathDef} {
            # -------------------------------------------------
            # http://www.selfsvg.info/?section=3.5
            # 
            # -------------------------------------------------
            # puts "\n\n=== Path2Line =====================\n"        
            # puts "       -> pathDef:\n             $pathDef\n"
            #
        set pen_x       0
        set pen_y       0
            #
        set ctr_x       0
        set ctr_y       0
            #
        set lastType    {}    
            #
        set pointList   {}
            #
        foreach {segmentType segmentCoords} $pathDef {
                #            
                # puts "       -----> $segmentType - $segmentCoords"
                # puts "                           - $pointList"
                #
            switch -exact $segmentType {
                M { # MoveTo 
                    lappend pointList  {*}$segmentCoords
                    set pen_x           [lindex $segmentCoords  end-1]
                    set pen_y           [lindex $segmentCoords  end]
                }
                L { # LineTo - absolute
                    lappend pointList  {*}$segmentCoords 
                    set pen_x           [lindex $segmentCoords  end-1]
                    set pen_y           [lindex $segmentCoords  end]
                } 
                C { # Bezier - absolute
                        #
                        # puts "     -> \$n_segmentCoords $n_segmentCoords"
                        #
                        #
                        # continue Bezier definition
                        #     - just a line to last coordinate
                        #                                
                    set segmentCoords       [linsert $segmentCoords 0 $pen_x $pen_y ]                            
                        #
                        # puts " --01-------> \$segmentCoords $segmentCoords"    
                        #
                    set bezierValues        [my Bezier $segmentCoords]
                        #
                        # puts " --02-------> \$bezierValues  $bezierValues"    
                        #
                    set ctr_x               [lindex $segmentCoords end-3]
                    set ctr_y               [lindex $segmentCoords end-2]
                        # puts "    --> $ctr_x - $ctr_y"
                        #
                    set pen_x               [lindex $segmentCoords end-1]
                    set pen_y               [lindex $segmentCoords end]
                        # puts "    --> $pen_x - $pen_y"
                        #
                    lappend pointList       {*}[lrange $bezierValues 2 end]
                        # set pointList          [concat $pointList [lrange $bezierValues 2 end]]
                }
                S { # Smooth Bezier - absolute
                        #
                        # exception on to less values
                        #     - just a line to last coordinate
                        #
                    set ctr_x               [lindex $segmentCoords end-3]
                    set ctr_y               [lindex $segmentCoords end-2]
                        #
                    switch -exact -- $lastType {
                        C -
                        S {
                            set vctLast         [::math::geometry::- [list $pen_x $pen_y] [list $lastCtr_x $lastCtr_y]]
                            set ctrlFirst       [::math::geometry::+ [list $pen_x $pen_y] $vctLast]
                            lassign $ctrlFirst  _x1 _y1 
                            # set segmentCoords   [linsert $segmentCoords 0 $pen_x $pen_y $_x1 $_y1]
                        }
                        M -
                        default {
                            # puts "   -> except control: $lastType <- lastType"
                            set _x1             $pen_x
                            set _y1             $pen_y
                            # set segmentCoords   [linsert $segmentCoords 0 $pen_x $pen_y $pen_x $pen_y]
                        }
                    }
                        #
                    set segCoords           [list $pen_x $pen_y]
                        #
                    foreach {_x2 _y2 x y} $segmentCoords {
                            # puts "    \$_x1  $_x1"
                            # puts "    \$_y1  $_y1"
                            # puts "    \$_x2  $_x2"
                            # puts "    \$_y2  $_y2"
                            # puts "    \$x    $x"
                            # puts "    \$y    $y"
                        lappend segCoords   $_x1 $_y1 $_x2 $_y2 $x $y
                        set pen_x           $x
                        set pen_y           $y
                        set vctLast         [::math::geometry::- [list $pen_x $pen_y] [list $_x2 $_y2]]
                        set ctrlFirst       [::math::geometry::+ [list $pen_x $pen_y] $vctLast]
                        lassign $ctrlFirst  _x1 _y1 
                    }
                        #
                    set bezierValues        [my Bezier $segCoords]
                        #
                    lappend pointList       {*}[lrange $bezierValues 2 end]
                        #
                    set pen_x               [lindex $segmentCoords end-1]
                    set pen_y               [lindex $segmentCoords end]
                        #
                }
                Z {
                    # puts "\n  ... this does not have an effect here: $segmentType"
                }
                default {
                    puts "\n\n  ... whats on there?  ->  $segmentType"
                    # puts     "   ...     segmentCoords  ->  $segmentCoords \n\n"
                }
            }
                #
                # puts " --03-------> \$pointList  $pointList"
                #
            set lastType    $segmentType
            set lastCtr_x   $ctr_x
            set lastCtr_y   $ctr_y
                #
        }
            #
        return $pointList
            #
    }
        #
    method Bezier {xy {PRECISION 10}} {
            # puts "           -> $xy"
        set PRECISION 16
        set np [expr {[llength $xy] / 2}]
        if {$np < 4} return
        
        set idx 0
        foreach {x y} $xy {
        set X($idx) $x
        set Y($idx) $y
        incr idx
        }

        set xy {}

        set idx 0
        while {[expr {$idx+4}] <= $np} {
            set a [list $X($idx) $Y($idx)]; incr idx
            set b [list $X($idx) $Y($idx)]; incr idx
            set c [list $X($idx) $Y($idx)]; incr idx
            set d [list $X($idx) $Y($idx)];# incr idx   ;# use last pt as 1st pt of next segment
            for {set j 0} {$j <= $PRECISION} {incr j} {
                set mu [expr {double($j) / double($PRECISION)}]
                set pt [my BezierPoint $a $b $c $d $mu]
                lappend xy [lindex $pt 0] [lindex $pt 1]
            }
        }
            # puts "             -> $xy"
        return $xy
            #
    }
        #
    method BezierPoint {a b c d mu} {
        # --------------------------------------------------------
        # http://www.cubic.org/~submissive/sourcerer/bezier.htm
        # evaluate a point on a bezier-curve. mu goes from 0 to 1.0
        # --------------------------------------------------------

        set  ab   [my LinePoint $a    $b    $mu]
        set  bc   [my LinePoint $b    $c    $mu]
        set  cd   [my LinePoint $c    $d    $mu]
        set  abbc [my LinePoint $ab   $bc   $mu]
        set  bccd [my LinePoint $bc   $cd   $mu]
        return    [my LinePoint $abbc $bccd $mu]
    }
        #
    method LinePoint {a b mu} {
        # -------------------------------------------------
        # http://www.cubic.org/~submissive/sourcerer/bezier.htm 
        # simple linear interpolation between two points (lerp)
        # -------------------------------------------------
        set ax [lindex $a 0]
        set ay [lindex $a 1]
        set bx [lindex $b 0]
        set by [lindex $b 1]
        return [list [expr {$ax + ($bx-$ax)*$mu}] [expr {$ay + ($by-$ay)*$mu}] ]
    }
        #
	method MirrorPoint {p a} {
            # reflects point p on line {{0 0} a}
    
        foreach {px py} $p  {ax ay} $a  break;

        if {$px == $ax && $py == $ay} {
            # return empty an handle on calling position if points are coincindent
            # puts "  .. hot wos";
            return {}
        }
        
        
        variable CONST_PI
            # puts "\n       ... start:      0 / 0  --   --  $px / $py  --  $ax / $ay"
   
        set alpha   [expr {atan2($ay,$ax)}]
            # puts "       ... \$alpha $alpha   [expr $alpha * 180 / $CONST_PI]"
        

        set beta    [expr {atan2($py,$px)}]
            # puts "       ... \$beta  $beta    [expr $beta * 180 / $CONST_PI]"


            # -- angular gap of a and p
        set delta   [expr {1.0*$beta - $alpha}]
            # puts "       ... \$delta $delta    [expr $delta  * 180 / $CONST_PI]"    


            # -- rotation mirrored point p
        set gamma   [expr {-2.0 * $delta}]
            # puts "       ... \$gamma $gamma    [expr $gamma  * 180 / $CONST_PI]"


            # -- new temporary position of p
            # puts "       ... temporary:  $px $py _____________________________"
        set xx [ expr {hypot($px,$py) * cos($beta + $gamma)}]
        set yy [ expr {hypot($px,$py) * sin($beta + $gamma)}]

        
            # puts "       ... temporary:  $xx $yy _____________________________"


        # -- new position of p
        set x [expr {$ax - $xx}]
        set y [expr {$ay - $yy}]
            # puts "       ... result:      $x $y"
        
            # puts "\n       ... start:      0 0  --  $x $y  --  $px $py  --  $ax $ay"
        return [list $x $y]                                        
    }
        #
    method ToShapeDict {_shapeDict} {
            #
            # puts "    -> $_shapeDict"
            #
        if {[dict exist $attrList id]} {
            set nodeID [dict get $attrList id]
        } else {
            set nodeID [clock microseconds]
        }
            #
        dict set shpDict id $nodeID    
            #
        set exportList {}
            #
        foreach fraction $_shapeDict {
                #
                # puts "    -> $fraction"
                #
            foreach {key pointList} $fraction {
                    # puts "   -> $key"
                    # puts "       -> $pointList"
                lappend exportList $key
                set _pointList [my TransformMatrix $pointList [$matrixObj get]]
                foreach {x y} $_pointList {
                    lappend exportList "$x,$y"
                }
            }
                #
        }
            #
        dict set shpDict d $exportList
            #
        return $shpDict
            #
    }
        #
        #
    method checkPoly__x {node} {
            #
        # variable trashNode
            #
        # puts ""
        puts "checkPoly__x:   -> $node"
        # puts ""
            #
            # points="52.43729999999999,-32.5785"
            #
            # set nodeName        [$node nodeName]
        set nodeName        [lindex $node 0]
        set nodeAttr        [lindex $node 1]
            # puts "   -> \$nodeName          $nodeName"
            #
        switch -exact $nodeName {
            polygon  -
            polyline {
                        # set pointAttribute  [$node getAttribute points]
                    set pointAttribute  [dict get $nodeAttr points]
                        # puts "   -> \$pointAttribute    $pointAttribute"
                    set pointCount      [llength $pointAttribute]
                        # puts "   -> \$pointCount        $pointCount"
                        #
                    if {$pointCount > 1} {
                        # puts "   -> \$node $node"
                        return $node
                    } else {
                        # puts "   -> \$trashNode $trashNode"
                        # puts "   -> \$node $node"
                        # $trashNode appendChild $node
                        return {} 
                    }
                }
            default {
                return $node
            }
        }
            #
    }
        #
        #
    method __AbsolutePath {pathDef} {
            #
            # https://www.w3.org/TR/SVG/paths.html#InterfaceSVGPathSeg
            #
        array \
            set penPosition { x 0 \
                              y 0 }
                                 
            #
            # puts "        a -> \$pathDef $pathDef"   
            #
            # -- loop throug pathDefinition
            #
        set pathList        {}
            #
        foreach {control valueList} $pathDef {
                #
                # puts "        b -> $control <- $valueList"
                # continue
            set _valueList {}
                #
            switch -exact $control {
                M {     #
                        # --- PATHSEG_MOVETO_ABS 
                        #
                    foreach {x y} $valueList {
                        set penPosition(x)      $x    ; incr listIndex 
                        set penPosition(y)      $y    ; incr listIndex
                    }
                    lappend pathList M $valueList
                }
                m {     #
                        # --- PATHSEG_MOVETO_REL 
                        #
                    foreach {x y} $valueList {
                        set penPosition(x)      [expr {$x + $penPosition(x)}] 
                        set penPosition(y)      [expr {$y + $penPosition(y)}]
                        lappend _valueList      $penPosition(x) $penPosition(y)
                    }
                    lappend pathList M $_valueList
                }
                L {     #
                        # --- PATHSEG_LINETO_ABS 
                        #
                    foreach {x y} $valueList  {
                        set penPosition(x)      $x
                        set penPosition(y)      $y
                    }
                    lappend pathList L $valueList
                }                    
                l {     #
                        # --- PATHSEG_LINETO_REL
                        #
                    foreach {x y} $valueList {
                        set penPosition(x)      [expr {$x + $penPosition(x)}] 
                        set penPosition(y)      [expr {$y + $penPosition(y)}]
                        lappend _valueList      $penPosition(x) $penPosition(y)
                    }
                    lappend pathList L $_valueList
                }
                C {     #
                        # --- PATHSEG_CURVETO_CUBIC_ABS
                        #
                    foreach {x y} $valueList  {
                        set penPosition(x)      $x
                        set penPosition(y)      $y
                        lappend _valueList      $penPosition(x) $penPosition(y)
                    }
                    lappend pathList C $_valueList
                }                    
                c {     #
                        # --- PATHSEG_CURVETO_CUBIC_REL
                        #
                    set bezierIndex             0
                            #
                    foreach {x y} $valueList {
                            # puts "       -> \$x $x"
                            # puts "       -> \$y $y"
                        set ctrlPosition(x)     [expr {$x + $penPosition(x)}]
                        set ctrlPosition(y)     [expr {$y + $penPosition(y)}]
                            # puts "           -> \$ctrlPosition(x) $ctrlPosition(x)"
                            # puts "           -> \$ctrlPosition(y) $ctrlPosition(y)"
                        lappend _valueList      $ctrlPosition(x) $ctrlPosition(y)
                            # puts "       -> \$bezierIndex $bezierIndex"
                            # puts "     -> \$valueList $valueList"
                            # puts "     -> \$_valueList $_valueList"
                        if {$bezierIndex > 1} {
                            # ... the first 2 points just inits the bezier
                            # puts "           -> \$bezierIndex $bezierIndex"
                            set penPosition(x)  $ctrlPosition(x) 
                            set penPosition(y)  $ctrlPosition(y)
                            set bezierIndex 0
                        } else {
                            incr bezierIndex
                        }
                            # parray penPosition
                    }
                    lappend pathList C $_valueList
                }
                Q {     #
                        # --- PATHSEG_CURVETO_QUADRATIC_ABS
                        #
                    puts "    $control  ... not implemented yet ($valueList)"
                }
                q {     #
                        # --- PATHSEG_CURVETO_QUADRATIC_REL
                        #
                    puts "    $control  ... not implemented yet ($valueList)"
                }
                A {     #
                        # --- PATHSEG_ARC_ABS 
                        #
                    puts "    $control  ... not implemented yet ($valueList)"
                }
                a {     #
                        # --- PATHSEG_ARC_REL
                        #
                    puts "    $control  ... not implemented yet ($valueList)"
                }
                H {     #
                        # --- PATHSEG_LINETO_HORIZONTAL_ABS 
                        #
                    set x                       $valueList
                    set penPosition(x)          $x
                    lappend _valueList          $penPosition(x) $penPosition(y)
                    lappend pathList            L $_valueList
                }
                h {     #
                        # --- PATHSEG_LINETO_HORIZONTAL_REL 
                        #
                    set x                       $valueList
                    set penPosition(x)          [expr {$x + $penPosition(x)}]
                    lappend _valueList          $penPosition(x) $penPosition(y)
                    lappend pathList            L $_valueList
                }
                V {     #
                        # --- PATHSEG_LINETO_VERTICAL_ABS 
                        #
                    set y                       $valueList
                    set penPosition(y)          $y
                    lappend _valueList          $penPosition(x) $penPosition(y)
                    lappend pathList            L $_valueList
                }
                v {     #
                        # --- PATHSEG_LINETO_VERTICAL_REL 
                        #
                    set y                       $valueList
                    set penPosition(y)          [expr {$y + $penPosition(y)}]
                    lappend _valueList          $penPosition(x) $penPosition(y)
                    lappend pathList            L $_valueList
                }
                S {     #
                        # --- PATHSEG_CURVETO_CUBIC_SMOOTH_ABS        
                    foreach {x y} $valueList {
                        set penPosition(x)      $x
                        set penPosition(y)      $y
                        lappend _valueList      $penPosition(x) $penPosition(y)
                    }
                    lappend pathList S $_valueList
                }
                s {     #
                        # --- PATHSEG_CURVETO_CUBIC_SMOOTH_REL
                        #
                        # puts "  s -> \$valueList $valueList"
                        #
                    set bezierIndex             0
                        #
                    foreach {x y} $valueList {
                            # puts "       -> \$x $x"
                            # puts "       -> \$y $y"
                        set ctrlPosition(x)     [expr {$x + $penPosition(x)}]
                        set ctrlPosition(y)     [expr {$y + $penPosition(y)}]
                            # puts "           -> \$ctrlPosition(x) $ctrlPosition(x)"
                            # puts "           -> \$ctrlPosition(y) $ctrlPosition(y)"
                        lappend _valueList      $ctrlPosition(x) $ctrlPosition(y)
                            #
                        if {$bezierIndex > 0} {
                            # ... the first point just inits the bezier
                            # puts "           -> \$bezierIndex $bezierIndex"
                            set penPosition(x)  $ctrlPosition(x) 
                            set penPosition(y)  $ctrlPosition(y)
                            set bezierIndex 0
                        } else {
                            incr bezierIndex
                        }
                    }
                        #
                    lappend pathList S $_valueList
                        #
                }
                T {     #
                        # --- PATHSEG_CURVETO_QUADRATIC_SMOOTH_ABS
                        #
                    puts "    $control  ... not implemented yet"
                }
                t {     #
                        # --- PATHSEG_CURVETO_QUADRATIC_SMOOTH_REL
                        #
                    puts "    $control  ... not implemented yet"
                }
                Z -
                z {     #
                        # PATHSEG_CLOSEPATH
                        # 
                    lappend pathList            Z {}
                }

                default {
                            puts "    $control  ... not registered yet"
                }    
            }
        
        }
            #
        set splitList   {}    
        set __splitList {}    
            #
        foreach {ctr value} $pathList {
            switch -exact $ctr {
                M {
                    if {$__splitList ne {}} {
                        lappend  splitList  $__splitList
                        set    __splitList  {}
                    }
                    lappend    __splitList  M    $value
                }
                default {
                    lappend    __splitList  $ctr $value
                }
            }
        }
        lappend  splitList  $__splitList    
            #
        if 0 {
            foreach segment $splitList {
                puts "              segment -> $segment"
            }
        }        
            #
            #
        return $splitList   
            #
     }
        #
    method __TransformMatrix {valueList matrix} {
        
            #
            #  -- the art of handling the transform matrix in SVG
            #           got it from here: http://commons.oreilly.com/wiki/index.php/SVG_Essentials/Matrix_Algebra
            #       | a  c  tx |                    | a*x  c*y  tx*1 |  -> x
            #       | b  d  ty |  *  |x  y  1|  =   | b*x  d*y  ty*1 |  -> y
            #       | 0  0   1 |                    | 0*x  0*y   1*1 |  -> z (not interesting in a 2D)   
            #
            # puts "    transform_SVGObject: $matrix"
        
            # puts "    TransformMatrix: $valueList"
            # puts "    TransformMatrix: [llength $valueList]"
            # puts "    TransformMatrix: $matrix"
        
        variable svgPrecision
        
        set valueList_Return {}
            
        foreach {a b c d e f} $matrix break

        foreach {x y} $valueList {
                #
                # puts "      -> \$x $x"    
                # puts "      -> \$y $y" 
            if {$y eq {}} continue
                # if {$y eq {}} {set y 0}
                #
            set x [expr {1.0 * $x}]
            set y [expr {1.0 * $y}]
                # puts "\n--------------------"
                # puts "       -> $x $y"
            set new_x [ expr {$a*$x + $c*$y + $e}]
            set new_y [ expr {$b*$x + $d*$y + $f}]
                # puts "          x/y:  $x / $y"
                # puts "          a/b -> xt:  $a / $b -> $new_x"
                # puts "          c/d -> yt:  $c / $d -> $new_y"
                #
            set valueList_Return [lappend valueList_Return $new_x $new_y]
                #
        }
            #
        return $valueList_Return
            #
    }
        #
        #
}        