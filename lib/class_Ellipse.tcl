 #
 # svgDOM
 #
 # https://www.w3.org/TR/SVG/shapes.html
 #
 #  2018.05.30 
 #
 #

oo::define svgDOM::Ellipse {
        #
        # https://www.w3.org/TR/SVG/shapes.html
        #
        # rect 
        # circle 
        # ellipse 
        # line 
        # polyline 
        # pointList         
        #
        #
    variable attrList matrixObj resolution    
    variable pointList 
    variable shapeDict                  ;# the returning node definition
            #
        #
    superclass svgDOM::SVGShape         ;# the template class of every element
        #
        #
    constructor {_attrList _matrixObj {_resolution 1}} {
            #
        set attrList    $_attrList
        set matrixObj   $_matrixObj
        set resolution  $_resolution
            #
            # puts "            -> Ellipse:   $attrList"
            # puts "                          $_matrixObj [$_matrixObj get]"
            #
        set pointList   {}
        set shapeDict   {}
            #
        next            ellipse $attrList $matrixObj
            #
        my parseShape
            # my toShapeDict    ;# ... is part of parseShape
            #
    }
        #
    destructor {
        puts "            -> [self] ... destroy SVGShape"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::Ellipse::$target_method $args  ... unknown"
    }
        #
    method parseShape {} {
            #
            # puts "   -parseShape-> $attrList"
            #
        if {[my checkSimplicity]} {
                # puts "\n\n"
                # puts "       ->   is simple"
                # puts "\n\n"
            my parseEllipse
        } else {
            my parsePolygon
        }
            #
        return
            #
    }
        #
    method checkSimplicity {} {
            #
            # puts "\n\n ---------------------------"
            # puts "  -> check checkSimplicity   is simple"
            # puts "             -> $attrList"
            # puts "             -> $matrixObj "
            # puts "                 -> [$matrixObj get]"
            #
        lassign [$matrixObj get] a b c d tx ty
            #           
        set a [format {%0.6f} $a]
        set b [format {%0.6f} $b]
        set c [format {%0.6f} [expr {-1.0 * $c}]]
        set d [format {%0.6f} $d]
            #
            # puts "                 -> $a $b $c $d"
            #
        if {$a == $d && $b == $c} {
            # puts "                     -> 1"
            return 1
        } else {
            # puts "                     -> 0"
            return 0    
        }
            #
    }    
        #
    method parsePolygon {} {
            #
            # puts "\n ---------------------------"
            # puts "       -> parsePolygon"
            #
        set cx  0
        set cy  0
            #
        if {[dict exists $attrList cx]} {
            set cx  [dict get $attrList cx]
        }
        if {[dict exists $attrList cy]} {
            set cy  [dict get $attrList cy]
        }
            #
        set rx      [dict get $attrList rx]
        set ry      [dict get $attrList ry]
            #
            # puts "    -> \$resolution    $resolution"
            #
        set pointList {}
        set PI  [expr {4*atan(1)}]
        set perimeter   [expr {$PI * ($rx + $ry) / 4}]      ; # approximate perimeter
        set perimeter   [expr {$perimeter * $resolution}]   ; # corrected perimeter by resolution
        set numSegment  [expr {$perimeter / 2.0}]           ; # segment length of 2.0
        if {$numSegment < 64} {
            set numSegment 64
        } else {
            set numSegment [expr {int($numSegment)}]
        }
            #
        set deltaAngle  [expr {2 * $PI / $numSegment}]
            # puts "   \$deltaAngle  $deltaAngle"
        set i 0
        while {$i < $numSegment} {
            set angle   [expr $i*$deltaAngle]
            set x       [expr $cx + ($rx * cos($angle))]
            set y       [expr $cy + ($ry * sin($angle))]
            lappend     pointList $x $y
            incr i
        }
            #
        set pointList   [my transform $pointList]
            #
            # puts "  -> \$pointList \n$pointList"    
            #
        set _pointList {}
        foreach  {x y} $pointList {
            lappend _pointList "$x,$y"
        }
        set _pointList  [join $_pointList { }]
            #
        set shapeDict   [list polygon [list points $_pointList]]
            #
    }    
        #
    method parseEllipse {} {
            #
            # puts "\n ---------------------------"
            # puts "       -> parseEllipse"
            #
        set cx  0
        set cy  0
            #
        if {[dict exists $attrList cx]} {
            set cx  [dict get $attrList cx]
        }
        if {[dict exists $attrList cy]} {
            set cy  [dict get $attrList cy]
        }
            #
        set rx      [dict get $attrList rx]
        set ry      [dict get $attrList ry]
            #
        set p_cnt   [my transform [list $cx $cy]]
        set p_x     [my transform [list [expr {$cx + $rx}] $cy]]
        set p_y     [my transform [list $cx [expr {$cy + $ry}]]]
            #
        lassign $p_cnt cx cy    
        set rx      [::math::geometry::distance $p_cnt $p_x] 
        set ry      [::math::geometry::distance $p_cnt $p_y] 
            #
        if {[format {%.3f} $rx] == [format {%.3f} $ry]} {
            set shapeDict   [list circle  [list cx $cx cy $cy r  $rx]]
        } else {
            set shapeDict   [list ellipse [list cx $cx cy $cy rx $rx ry $ry]]
        }
            #
    }    
        #
    method transform {pointList} {
            #
            # pointList:   {x y  x y  x y  x y  ...}
            #
            #  -- the art of handling the transform matrix in SVG
            #           got it from here: http://commons.oreilly.com/wiki/index.php/SVG_Essentials/Matrix_Algebra
            #       | a  c  tx |                    | a*x  c*y  tx*1 |  -> x
            #       | b  d  ty |  *  |x  y  1|  =   | b*x  d*y  ty*1 |  -> y
            #       | 0  0   1 |                    | 0*x  0*y   1*1 |  -> z (not interesting in a 2D)   
            #
            # puts "    transform_SVGObject: $matrix"
            #
            #
            # puts "        -> transform: $pointList"
            #
        set pointList_new   {}
            #
            # puts "       -> 090: [$matrixObj get]"
            #
        lassign [$matrixObj get]    a b c d tx ty
            #
        foreach {x y} $pointList {
                #
            set x [expr {1.0 * $x}]
            set y [expr {1.0 * $y}]
                # puts "\n--------------------"
                # puts "       -> $x $y"
            set new_x [ expr {$a*$x + $c*$y + $tx}]
            set new_y [ expr {$b*$x + $d*$y + $ty}]
                # puts "          x/y:  $x / $y"
                # puts "          a/b -> xt:  $a / $b -> $new_x"
                # puts "          c/d -> yt:  $c / $d -> $new_y"
            lappend pointList_new   $new_x $new_y
                #
        }
            #
            # puts "       -> 099: $pointList_new"    
            #
        return $pointList_new
            #
    }
        #
    method _remove_getPolygon {{mode transformed}} {
    
        puts "  -> getPolygon"
        exit
        set _polygon    [my transform $pointList]
        return $_polygon
    }
        #
}        