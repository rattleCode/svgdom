 #
 # svgDOM
 #
 #   2018.05.30 
 #
 #

oo::define svgDOM::SVGMatrix {
        #
        # https://www.w3.org/TR/SVG/coords.html#InterfaceSVGMatrix
        #
        # SVGMatrix multiply(in SVGMatrix secondMatrix);
        # SVGMatrix inverse() raises(SVGException);
        # SVGMatrix translate(in float x, in float y);
        # SVGMatrix scale(in float scaleFactor);
        # SVGMatrix scaleNonUniform(in float scaleFactorX, in float scaleFactorY);
        # SVGMatrix rotate(in float angle);
        # SVGMatrix rotateFromVector(in float x, in float y) raises(SVGException);
        # SVGMatrix flipX();
        # SVGMatrix flipY();
        # SVGMatrix skewX(in float angle);
        # SVGMatrix skewY(in float angle);
        #
    variable  toRad
    variable  matrix  a b c d e f  baseMatrix
        #
    constructor {} {
            #
            #      |a c e|
            #      |b d f|
            #      {a b c d e f}
        set a       1.0
        set b       0.0
        set c       0.0
        set d       1.0
        set e       0.0
        set f       0.0
            #
        set baseMatrix  [list $a $b $c $d $e $f]
        set matrix      [list $a $b $c $d $e $f]
            #
        set toRad       [expr {4*atan(1)/180}]    
            #
    }
    destructor { 
        puts "            -> [self] ... destroy SVGMatrix"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... svgDOM::SVGMatrix::$target_method $args  ... unknown"
    }
        #
    method transform {cmdList} {
            #
            # https://www.w3.org/TR/SVG/coords.html#TransformAttribute
            # https://nikit.tcl-lang.org/page/svg2can
            #
        puts "<D> transform $cmdList"
            #
        if {[llength $cmdList] == 0} {
            return $matrix
        } else {
                # puts "                    ---> $cmdList"
            foreach {key value} $cmdList {
                    # puts "                      -> $key <-- $value"
                switch -exact -- $key {
                    matrix {
                        my multiply     $value
                    }
                    translate {
                        my translate    $value
                    }
                    scale {
                        my scale        $value 
                    }
                    rotate {
                        my rotate       $value 
                    }
                    skewX {  
                        my skewX        $value 
                    }
                    skewY {      
                        my skewY        $value 
                    }
                    default {
                        # ... do nothing
                    }
                }
            }
                #    
            return $matrix
                #
        } 
            #
    }
        #
    method multiply {matrix2 {matrixOrder append}} {
            #
            # matrixOrder
            #   prepend 
            #       Specifies that the new matrix is on the left and the existing matrix is on the right.
            #   append (default)
            #       Specifies that the existing matrix is on the left and the new matrix is on the right.
            #
            # puts "    <D> multiply"
            # puts "                -> multiply $matrix"
            # puts "                         -> $matrix2"
            #
        if {$matrixOrder eq "append"} {
            lassign $matrix     a1 b1 c1 d1 tx1 ty1 ;# ... $matrix: the global variable
            lassign $matrix2    a2 b2 c2 d2 tx2 ty2
        } else {
            lassign $matrix     a2 b2 c2 d2 tx2 ty2 ;# ... $matrix: the global variable       
            lassign $matrix2    a1 b1 c1 d1 tx1 ty1
        }
            #
        set matrix [list \
                [expr {$a1 * $a2  + $c1 * $b2}]       \
                [expr {$b1 * $a2  + $d1 * $b2}]       \
                [expr {$a1 * $c2  + $c1 * $d2}]       \
                [expr {$b1 * $c2  + $d1 * $d2}]       \
                [expr {$a1 * $tx2 + $c1 * $ty2 + $tx1}] \
                [expr {$b1 * $tx2 + $d1 * $ty2 + $ty1}]]
            #
        lassign $matrix  a b c d e f            ;# ... a b c d e f: the global variable
            #
        # puts "      -> multiply: $matrix"    
            #
    }
        #
    method translate {value} {
        puts "    <D> translate $value"
            #
        if {[llength $value] == 1} {
            set m   [list 1 0 0 1 $tx 0]
        } else {
            lassign $value tx ty
            set m   [list 1 0 0 1 $tx $ty]
        }
        my multiply $m
    }
        #
    method scale {value} {
        puts "    <D> scale $value"
        if {[llength $value] == 1} {
            set m   [list $value 0 0 $value 0 0]
        } else {
            lassign $value sx sy
            set m   [list $sx 0 0 $sy 0 0]
        }
        my multiply $m 
    }
        #
    method rotate {value} {
        puts "    <D> rotate $value <-   [llength $value]"
        lassign $value angle x y
        set angleRad [expr {-1.0 * $angle * $toRad}]
        # puts "             -> $angleRad"
        set m [list \
                    [expr {cos($angleRad)}] [expr {-1.0*sin($angleRad)}] \
                    [expr {sin($angleRad)}] [expr {cos($angleRad)}] \
                    0 0]
        my multiply $m 
    }
        #
    method skewX {value} {
        puts "    <D> skewX $value"
        set angleRad [expr {$value * $toRad}]
            # puts "             -> $angleRad"
        set m [list \
                    1 0 \
                    [expr {tan($angleRad)}] 1 \
                    0 0]       
        my multiply $m 
    }
        #
    method skewY {value} {
        puts "    <D> skewY $value"
        set angleRad [expr {$value * $toRad}]
            # puts "             -> $angleRad"
        set m [list \
                    1 [expr {tan($angleRad)}] \
                    0 1 \
                    0 0]       
        my multiply $m 
    }
        #
    method get {} {
        return $matrix
    }
        #
    method applyPointList {pointList} {
    
    }    
        #
    method Matrix2Command {_matrix} {
            #
            #
        set initPolyline    {0 0  0 10  10 10}
            #
        lassign $initPolyline   x1_p1 y1_p1 x1_p2 y1_p2 x1_p3 y1_p3
            #
        puts "      -> init:    $x1_p1 $y1_p1 - $x1_p2 $y1_p2 - $x1_p3 $y1_p3"    
            #
            #
        set resultPolyline  [my matrixTransform $initPolyline $_matrix]    
            #
        lassign $resultPolyline x2_p1 y2_p1 x2_p2 y2_p2 x2_p3 y2_p3
            #
        puts "      -> result:  $x2_p1 $y2_p1  - $x2_p2 $y2_p2 - $x2_p3 $y2_p3"    
            #
            #
            #
            # -- translate -------
        set translate   [list $x2_p1 $y2_p1]    
        puts "          -> \$translate :    $translate"       
            #
            # -- translate -------
        
        set translate_0 [list [expr {-1.0 * $x2_p1}] [expr {-1.0 * $y2_p1}]]
        puts "          -> \$translate_0 :  $translate_0"       
            #
            # -- rotate ----------
        set rotate      [expr {1.0 * [::math::geometry::angle [list $x2_p2 $y2_p2 $x2_p3 $y2_p3]]}]
        puts "          -> \$rotate :       $rotate"       
        if {$rotate < 0} {
            set rotate  [expr {$rotate + 360}]
        }
        puts "          -> \$rotate :       $rotate"       
            #
            # -- skewX -----------
        set skewX       [expr {[::math::geometry::angle [list $x2_p2 $y2_p2 $x2_p3 $y2_p3]] - [::math::geometry::angle [list $x2_p2 $y2_p2 $x2_p1 $y2_p1]]} - 90]
        if {$skewX < 0} {
            set skewX   [expr {$skewX + 360}]
        }
        puts "          -> \$skewX :        $skewX"       
            #
            # -- scaleX ----------
        set length      [::math::geometry::distance [list $x2_p2 $y2_p2] [list $x2_p3 $y2_p3]] 
        set scaleX      [expr {$length / 10}] 
        puts "          -> \$scaleX :        $scaleX"       
            #
            # -- scaleY ----------
        set distance    [::math::geometry::calculateDistanceToLine [list $x2_p1 $y2_p1] [list $x2_p2 $y2_p2  $x2_p3 $y2_p3]]
        set scaleY      [expr {$distance / 10}] 
        puts "          -> \$scaleY :        $scaleY"       
            #
        return [list \
                    skewX       $skewX \
                    scale       [list $scaleX $scaleY] \
                    rotate      [list $rotate 0 0] \
                    translate   $translate \
                    ]
    }
        
        
        #
}
