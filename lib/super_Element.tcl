#
# svgDOM
#
# tclSVG -- an oo hierarchy to parse, mutate and generate SVG
#
#   2018.05.30
#
#       https://wiki.tcl.tk/25998
#       https://wiki.tcl.tk/3650?redir=3654  -> Colin McCormack
#
#

    #
oo::define svgDOM::Element {
        #
    variable  root parent node
    variable  id nodeName children  
    variable  attrList matrix shape style
    variable  exportNode
        #
    constructor {_root _parent _node} {
            #
        set id              {}
        set root            $_root              ;# we need to know our root SVG-Element object
        set parent          $_parent            ;# we need to know our parent SVG-Element object
        set children        {}                  ;# we still do not know now
        set node            $_node              ;# we need to know our tdom node
        set nodeName        [$node nodeName]    ;# we need to know our node type
            #
        set matrix          {}
        set shape           {}    
        set style           {}    
            #
        my update
            #
            # my report    
            #
    }
        #
    destructor {
        foreach o [my children] {
            catch {$o destroy}        ;# if we have children, destroy them
        }
        catch {
            $matrix destroy
        }
        catch {
            # remove interp-wide node->object mapping
            # $doc del_node2obj $node
        }
        catch {
            # remove document-wide id->object mapping
            # $doc del_id2obj [my attr id]
        }
    }
        #
    method unknown {target_method args} {
        puts "<E> ... svgDOM::Element $target_method $args  ... unknown"
    }    
        #
        #
    method update {} {
            #
            #
            # puts "  -- here I am -- $nodeName -- 100 --"
            # --- id -----------------------
            #
        if {[$node hasAttribute id]} {
            set nodeID  [$node getAttribute id]
                # puts "\n--- 002 --- \$nodeName $nodeName: \$nodeID $nodeID\n"
            set nodeID  [$root unique_id $nodeID]
            if {[$root exists $nodeID]} {
                # set nodeID  [$root unique_id $nodeID]
            }
        } else {
            set nodeID  [$root unique_id]
        }
        
            # puts "   -> \$nodeID  $nodeID"
            #
        set id  $nodeID    
            #
            #
            # --- register -----------------
            #
        $root set_id2obj $nodeID [self]
            #
            #
            # --- nodeName -----------------
            #
            # puts "   ----> [$_node nodeName]"
        set nodeName    [$node nodeName]
            #
            #
            # --- attributes ---------------
            #
            # puts "   ----> [$_node attributes]"
        set attrList {}
        foreach key [$node attributes] {
            # puts "   ----> $key"
            catch {dict set attrList $key [$node getAttribute $key]}
        }
            #
        return
            #
    }    
        #
        #
    method buildChild {_root _parent _node args} {
            #
        # puts "\n"    
        # puts "  1 -> $_root - $_node - $args"    
        # puts "    2   -> [$_node attributes]"    
            #
                    #
        if {[$_node nodeType] eq "ELEMENT_NODE"} {
                #
                # puts "--- 001 ---"
                # puts "\n ----------------"    
                # puts "       create -> [$_node nodeName]"
                #
            switch -exact -- [$_node nodeName] {
                g -
                rect -
                circle -
                ellipse -
                line -
                polyline -
                polygon -
                path -
                default {
                    set obj [svgDOM::SVGElement new $_root [self] $_node]
                        # puts "--- 002 ---"
                        #
                        # --- children -------------
                        #
                    foreach childNode [$_node childNodes] {
                            # puts "--- 004 ---  [$childNode getAttribute id]"
                        set childObj    [$obj buildChild $_root $obj $childNode]
                        if {$childObj ne {}} {
                            $obj appendChild $childObj
                        }
                            #
                    }
                        #
                    return $obj
                        #
                }
            }
                #
        } else {
                # puts "--- 002 ---"
            return ""
                #
        }
            #
            #
    }
        #
        #
    method TransformAttrToList {transformAttr} {
            # 
            # ... Copyright (c) 2004-2007  Mats Bengtsson
            # ... proc svg2can::TransformAttrToList {cmd}
            #
        # puts "\n============================================="
        # puts "\n"
        # puts " -----> TransformAttrToList:        $transformAttr"
        # puts "    -------------------------------------------"
            #
        set _cmdList [split [string trim $transformAttr] )]
            #
            # puts "  -> \$_cmdList $_cmdList"
            #
        set cmdList {}
            #
        foreach cmd $_cmdList {
                # puts "        -> $cmd"
            lassign [string trim [split $cmd (]] key value
                # puts "             -> $key  - $value"
            set key     [string trim $key]
            set value   [join [split $value ,]]
                # puts "             -> $key  - $value"
            if {$value ne {}} {
                lappend cmdList $key $value
            }
        }
            #
            # puts "\n\n  -> \$cmd  $cmdList\n\n"
            #
        return $cmdList   
            #
    }        
        #
        # set/get id of object
    method id {args} {
        if {[llength $args] == 0} {
            return $id
        } else {
                # puts "     ... set id $args"
            set id [lindex $args 0]
            return $id
        }
    }
        #
        # get/set object parent
    method parent {args} {
        if {[llength $args] == 0} {
            return $parent
        } else {
                # puts "     ... set id $args"
            set parent [lindex $args 0]
            return $parent
        }
    }
        #
        # get/set object matrix
    method matrix {} {
        return $matrix
    }
        #
        # get/set object matrix
    method style {} {
        return $style
    }
        #
        # get/set object children
    method appendChild {obj} {
        if {$obj ne {}} {
            lappend children $obj
        }
    }
        #
        #
    method children {args} {
        if {[llength $args] == 0} {
            return $children
        } else {
                # puts "     ... set id $args"
            set children $args
            return $children
        }
    }
        #
        #
    method getAttributeList {} {
        return $attrList
    }
        #
        # get/set object attribute(s)
    method attribute {args} {
        switch [llength $args] {
            1 {
                return [set [lindex $args 0]]
            }
            2 {
                set $args
                return [set [lindex $args 0]]
            }
            default {
                return [list \
                        id $id \
                        root $root \
                        parent $parent \
                        children $children \
                        node $node ]
            }
        }
    }
        #
        # report the svg object structure
    method report {} {
            #
        puts ""
        puts "   --> $nodeName  - $id -- [self]"
        puts "         ->     parent     [my parent] [[[my parent] matrix] get]"
        puts "         ->     matrix     [my matrix] [$matrix get] "
        puts "         ->     style      [my style]  [$style get] "
        foreach child ${children} {
            $child report
        }
            # puts "   >${children}<"
            #
    }        
        #
}
