 ########################################################################
 #
 # test_cad4tcl.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2017/11/26
 #
 #


set WINDOW_Title      "cad4tcl, an extension for canvas"
    #
    #
puts "\n"
    #
set BASE_Dir  [file normalize [file dirname [file normalize $::argv0]]]
puts "   \$BASE_Dir ............... $BASE_Dir"
    #
set TEST_ROOT_Dir [file normalize [file dirname [lindex $argv0]]]
puts "   \$TEST_ROOT_Dir .......... $TEST_ROOT_Dir"
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
    #
set TEST_Sample_Dir [file join $TEST_ROOT_Dir _sample]
puts "    -> \$TEST_Sample_Dir .... $TEST_Sample_Dir"
    #
set TEST_Debug_Dir [file join $TEST_ROOT_Dir _debug]
puts "    -> \$TEST_Debug_Dir ..... $TEST_Debug_Dir"
    #
set TEST_Export_Dir [file join $TEST_ROOT_Dir __export]
puts "    -> \$TEST_Export_Dir .... $TEST_Export_Dir"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir ext-tcllib]
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
    #
foreach dir $tcl_library {
    puts "   -> tcl_library $dir"
}    
    #
foreach dir $auto_path {
    puts "   -> auto_path   $dir"
}    
    #
    #
package require     Tk
package require     cad4tcl
package require     vectormath
    #
    #
variable w_textReport    {}
    #
set cad4tcl::canvasType   1
    #
 
 	
    ##+######################
proc create_config_line {w lb_text entry_var start end  } {		
    frame   $w
    pack    $w

    global $entry_var

    label   $w.lb	-text $lb_text            -width 20  -bd 1  -anchor w 
    entry   $w.cfg	-textvariable $entry_var  -width 10  -bd 1  -justify right -bg white 
 
    scale   $w.scl	-width        12 \
                    -length       120 \
                    -bd           1  \
                    -sliderlength 15 \
                    -showvalue    0  \
                    -orient       horizontal \
                    -command      "sketchboard::update_board" \
                    -variable     $entry_var \
                    -from         $start \
                    -to           $end 
                        # -resolution   $resolution

    pack      $w.lb  $w.cfg $w.scl    -side left  -fill x		    
    bind      $w.cfg <Leave>  "::updateValue $entry_var"
    bind      $w.cfg <Return> "::updateValue $entry_var"
}
proc create_status_line {w lb_text entry_var} {	     
        frame   $w
        pack    $w
 
        global $entry_var

        label     $w.lb     -text $lb_text            -width 20  -bd 1  -anchor w 
        entry     $w.cfg    -textvariable $entry_var  -width 10  -bd 1  -justify right -bg white 
        pack      $w.lb  $w.cfg    -side left  -fill x		    
}
proc updateValue {varName args} {
    puts "   ... updateValue:  $varName $args"
    catch {sketchboard::update_board}
}
    ##+######################
    
    
variable PI   [expr {4*atan(1)}]

proc to_rad {{angle 0}} {
    return [expr {$angle * $::PI / 180}]
}
proc rotate {x  y  rad} {
        set X [expr {$x * cos($rad) - $y * sin($rad)}]
        set Y [expr {$x * sin($rad) + $y * cos($rad)}]
        return [list x $X  y $Y]
    }
proc get_arc_Cubic {x1 y1 rx ry angle large_arc_flag sweep_flag x2 y2 {recursive {}}} {
      # for more information of where this math came from visit:
      # http://www.w3.org/TR/SVG11/implnote.html#ArcImplementationNotes
      
    puts "    -> get_arc_Cubic "
    puts "      -> \$x1      $x1  "
    puts "      -> \$y1      $y1  "
    puts "      -> \$rx      $rx  "
    puts "      -> \$ry      $ry  "
    puts "      -> \$angle   $angle"
    puts "      -> \$large_arc_flag $large_arc_flag"    ;# 1 -> longer arc segemnt
    puts "      -> \$sweep_flag     $sweep_flag"    ;# 1 -> "positive-angle" direction
    puts "      -> \$x2      $x2"
    puts "      -> \$y2      $y2"
    puts "      -> \$recursive      $recursive"
    
    set xy_1 "$x1, $y1"
    set xy_2 "$x2, $y2"
      
    variable PI
    set _120 [expr {$PI * 120.0 / 180}]
    set res  ""
    set xy ""

        
        
    puts ""     
    puts "    \$rx $rx"     
    puts "    \$ry $ry"     
    if {$rx == 0 || $ry == 0} {
        puts "   ... exception 01"
        return [list $x1 $y1 $x2 $y2 $x2 $y2];
    }
    puts "    \$recursive >$recursive<"
    if {$recursive eq {}} {
        puts    "--- <D>"
        set xy  [rotate $x1 $y1  [to_rad [expr -1.0 * $angle]]]
        puts "$xy"
        set x1  [dict get $xy x]
        set y1  [dict get $xy y]
        set xy  [rotate $x2 $y2 [to_rad [expr -1.0 * $angle]]]
        puts "$xy"
        set x2  [dict get $xy x]
        set y2  [dict get $xy y]
        set cos [expr {cos($angle * $PI / 180)}]
        set sin [expr {sin($angle * $PI / 180)}]
        set x   [expr {($x1 - $x2) / 2.0}]
        set y   [expr {($y1 - $y2) / 2.0}]
        set h   [expr {$x * $x / ($rx * $rx) + $y * $y / ($ry * $ry)}]
        if {$h > 1} {
            set h   [expr {sqrt($h)}]
            set rx  [expr {$h * $rx}]
            set ry  [expr {$h * $ry}]
        }
        set rx2 [expr {$rx * $rx}]
        set ry2 [expr {$ry * $ry}]
        
        if {$large_arc_flag == $sweep_flag} {
            #set k = (large_arc_flag == sweep_flag ? -1 : 1) *
            set _koeff -1
        } else {
            set _koeff  1
        }
            #  
        set k   [expr {$_koeff * sqrt(abs(($rx2 * $ry2 - $rx2 * $y * $y - $ry2 * $x * $x) / ($x2 * $y * $y + $ry2 * $x * $x)))}]
        set cx  [expr {$k * $rx * $y / $ry + ($x1 + $x2) / 2.0}]
        set cy  [expr {$k * -1.0 * $ry * $x / $rx + ($y1 + $y2) / 2.0}]
        set f1  [expr {asin(($y1 - $cy) / $ry)}]   ;# toFixed(9)
        set f2  [expr {asin(($y2 - $cy) / $ry)}]   ;# toFixed(9)
            #
        if {$x1 < $cx} {set f1 [expr {$::PI - $f1}]}
        if {$x2 < $cx} {set f1 [expr {$::PI - $f1}]}
        # f1 = x1 < cx ? PI - f1 : f1;
        # f2 = x2 < cx ? PI - f2 : f2;
        if {$f1 < 0} {set f1 [expr {$::PI * 2 + $f1}]}
        if {$f2 < 0} {set f2 [expr {$::PI * 2 + $f2}]}
        
        if {$sweep_flag && $f1 > $f2} {
            set f1  [expr {$f1 - $PI * 2}]
        }
        if {!$sweep_flag && $f2 > $f1} {
            set f2 [expr {$f2 - $PI * 2}]
        }
        
    } else {
        puts "    <D> 002"
        set f1 [lindex $recursive 0]
        set f2 [lindex $recursive 1]
        set cx [lindex $recursive 2]
        set cy [lindex $recursive 3]
    }
    
    puts "   ... return 01"
    puts "   ... cx  $cx"
    puts "   ... cy  $cy"
    puts "   ... f1  $f1"
    puts "   ... f2  $f2"
    puts "   ... return 01"
    
    return "$xy_1 $cx,$cy $xy_2"
    
    
    
    
    return
    var df = f2 - f1;
    if (abs(df) > _120) {
        var f2old = f2,
            x2old = x2,
            y2old = y2;
        f2 = f1 + _120 * (sweep_flag && f2 > f1 ? 1 : -1);
        x2 = cx + rx * math.cos(f2);
        y2 = cy + ry * math.sin(f2);
        res = a2c(x2, y2, rx, ry, angle, 0, sweep_flag, x2old, y2old, [f2, f2old, cx, cy]);
    }
    df = f2 - f1;
    var c1 = math.cos(f1),
        s1 = math.sin(f1),
        c2 = math.cos(f2),
        s2 = math.sin(f2),
        t = math.tan(df / 4),
        hx = 4 / 3 * rx * t,
        hy = 4 / 3 * ry * t,
        m1 = [x1, y1],
        m2 = [x1 + hx * s1, y1 - hy * c1],
        m3 = [x2 + hx * s2, y2 - hy * c2],
        m4 = [x2, y2];
    m2[0] = 2 * m1[0] - m2[0];
    m2[1] = 2 * m1[1] - m2[1];
    if (recursive) {
        return [m2, m3, m4].concat(res);
    } else {
        res = [m2, m3, m4].concat(res).join().split(",");
        var newres = [];
        for (var i = 0, ii = res.length; i < ii; i++) {
            newres[i] = i % 2 ? rotate(res[i - 1], res[i], rad).y : rotate(res[i], res[i + 1], rad).x;
        }
        return newres;
    }
}

    
    
    
    
    
    
    

namespace eval sketchboard {
        #
    variable cvObject
        # defaults
        
    variable x1                30     
    variable y1                 0   
    variable rx                50     
    variable ry                25   
    variable angle              0
    variable x2               170   
    variable y2                10   
        
    variable large_arc_flag     0
    variable sweep_flag         1

    variable std_fnt_scl        1
    variable font_colour	   black
    variable demo_type		   dimension
    variable drw_scale		    1
    variable cv_scale		    5
        #
    proc createStage {cv_path cv_width cv_height st_width st_height unit st_scale args} {
        variable cvObject
        variable cv_scale
        set cvObject    [cad4tcl::new  $cv_path  $cv_width  $cv_height  A4  1.0  40]
        set w           [$cvObject getCanvas]    
        set cv_scale    [$cvObject configure Canvas Scale]
        update_board
        return $cvObject
    }
    proc reportDragObject {args} {
        puts "-> reportDragObject:\n      $args"
        set w $::w_textReport
        $w delete 1.0  end 
        $w insert end "-> reportDragObject:\n      $args\n"
    }
    proc update_board {{value {0}} args} {
            #
        variable  cvObject
            #
        variable x1                 
        variable y1               
        variable rx                 
        variable ry               
        variable angle              
        variable x2               
        variable y2               
            #
        variable large_arc_flag
        variable sweep_flag    
            #
        puts "-> update_board:\n      $args"
            #
        $cvObject deleteContent
            #
        set Origin_00   {50 100}
            #
        set pos_Start   [vectormath::addVector $Origin_00 [list $x1 $y1]]
        set pos_End     [vectormath::addVector $Origin_00 [list $x2 $y2]]
            #
        set obj_Start   [$cvObject create   circle  $pos_Start   -radius 2  -tags {posStart}  -fill lightblue  -outline blue  -width 1]
        set obj_End     [$cvObject create   circle  $pos_End     -radius 2  -tags {posEnd}    -fill lightblue  -outline blue  -width 1]
            #
            
        lassign $pos_Start  x1_abs  y1_abs     
        lassign $pos_End    x2_abs  y2_abs     
            #
        puts "   -> \$x1_abs $x1_abs"
        puts "   -> \$y1_abs $y1_abs"
        puts "   -> \$x2_abs $x2_abs"
        puts "   -> \$y2_abs $y2_abs"
            #
        set obj_Arc     [$cvObject create   path   "M $x1_abs $y1_abs    \
                                                    A $rx $ry   $angle   $large_arc_flag  $sweep_flag    $x2_abs  $y2_abs" \
                                            -stroke darkorange  -strokewidth 2]

        if {$large_arc_flag eq 1} {set large_arc_flag_2 0} else {set large_arc_flag_2 1}                                   
        if {$sweep_flag     eq 1} {set sweep_flag_2     0} else {set sweep_flag_2     1}                                   
        set obj_Arc     [$cvObject create   path   "M $x1_abs $y1_abs    \
                                                    A $rx $ry   $angle   $large_arc_flag_2  $sweep_flag_2    $x2_abs  $y2_abs" \
                                            -stroke blue  -strokewidth 2] 

        set defPos      [::get_arc_Cubic   $x1_abs $y1_abs   $rx $ry   $angle   $large_arc_flag $sweep_flag    $x2_abs $y2_abs]

        set polyLine [join [split $defPos ,]]
        set item_102 [$cvObject create line \
                        $polyLine \
                        -fill red -width 1]

            #
            #            
            #
        set y2_svg [expr {$y1_abs - ($y2_abs - $y1_abs)}]
            #
        set filename "$::BASE_Dir/test.svg"
            # open the filename for writing
        set fileId [open $filename "w"]
            # send the data to the file -
            #  omitting '-nonewline' will result in an extra newline
            # at the end of the file
        puts -nonewline $fileId  "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
        puts -nonewline $fileId  ""
        puts -nonewline $fileId  "<svg xmlns=\"http://www.w3.org/2000/svg\"\n"
        puts -nonewline $fileId  "width=\"297mm\" height=\"210mm\"\n" 
        puts -nonewline $fileId  "viewBox=\"0 0 297 210\"\n"
        puts -nonewline $fileId  "version=\"1.1\">\n"
        puts -nonewline $fileId  ""
        puts -nonewline $fileId  "   <path d=\"M  $x1_abs,$y1_abs   A $rx,$ry   $angle   $large_arc_flag,$sweep_flag    $x2_abs,$y2_svg\""
        puts -nonewline $fileId  "        stroke=\"darkorange\"  strokewidth=\"2\"  fill=\"none\"/>\n"
        puts -nonewline $fileId  "   <path d=\"M  $x1_abs,$y1_abs   A $rx,$ry   $angle   $large_arc_flag_2,$sweep_flag_2    $x2_abs,$y2_svg\""
        puts -nonewline $fileId  "        stroke=\"blue\"  strokewidth=\"2\"  fill=\"none\"/>\n"
        puts -nonewline $fileId  "</svg>\n"
                                 
            # close the file, ens"</svg>\n"uring the data is written out before you continue
            #  with processing.
        close $fileId
            #
        set dragObj_1   [$cvObject create   circle  	{10 10}  -radius 2  -tags {Line_01}   -fill lightgray  -outline gray  -width 1]
        $cvObject registerDragObject $dragObj_1 [list [namespace current]::reportDragObject]
            #
        set w $::w_textReport
        puts "  -> \$w $w"
        if {$w != {}} {
            $w delete 1.0  end 
            
            $w insert end "\n"
                #
            $w insert end "<path d=\"M  $x1_abs,$y1_abs   A $rx,$ry   $angle   $large_arc_flag,$sweep_flag    $x2_abs,$y2_svg\""
            $w insert end "     stroke=\"darkorange\"  trokewidth=\"2\"  />\n"
                #
        }
            #
        set dragObj_1   [$cvObject create   circle  	{10 10}  -radius 2  -tags {Line_01}   -fill lightgray  -outline gray  -width 1]
        $cvObject registerDragObject $dragObj_1 [list [namespace current]::reportDragObject]
            #
    }
    proc demo_cad4tcl {} {
        variable  cvObject
    }
    proc recenter_board {} {
        variable  cvObject
        variable  cv_scale 
        variable  drw_scale 
        puts "\n  -> recenter_board:   $cvObject "
        puts "\n\n============================="
        puts "   -> cv_scale:          	$cv_scale"
        puts "   -> drw_scale:          $drw_scale"
        puts "\n============================="
        puts "\n\n"
        set cv_scale [$cvObject center $cv_scale]
    }
    proc refit_board {} {
        variable  cvObject
        variable  cv_scale 
        variable  drw_scale 
        puts "\n  -> recenter_board:   $cvObject "
        puts "\n\n============================="
        puts "   -> cv_scale:          	$cv_scale"
        puts "   -> drw_scale:          $drw_scale"
        puts "\n============================="
        puts "\n\n"
        set cv_scale [$cvObject fit]
    }
    proc scale {{value {1}}} {
        variable  cvObject
        variable  cv_scale 
        variable  drw_scale 
        puts "\n  -> scale_board:   $cvObject"
        puts "\n\n============================="
        puts "   -> cv_scale:          	$cv_scale"
        puts "   -> drw_scale:          $drw_scale"
        puts "\n============================="
        set cv_scale [expr $cv_scale * $value]
        puts "\n\n============================="
        puts "   -> cv_scale:          	$cv_scale"
        puts "   -> drw_scale:          $drw_scale"
        puts "\n============================="
        puts "\n\n"
        $cvObject center $cv_scale
    }
    proc scale_board {{value {1}}} {
        variable  cvObject
        variable  cv_scale 
        variable  drw_scale 
        puts "\n  -> scale_board:   $cvObject"
        puts "\n\n============================="
        puts "   -> cv_scale:          	$cv_scale"
        puts "   -> drw_scale:          $drw_scale"
        puts "\n============================="
        puts "\n\n"
        $cvObject center $cv_scale
    }
    proc scale_view {{value {1}}} {
        variable  cvObject
        variable  view_scale 
        variable  cv_scale 
        puts "\n  -> scale_view:   $cvObject"
        puts "\n\n============================="
        puts "   -> cv_scale:          	$cv_scale"
        puts "   -> view_scale:         $view_scale"
        puts "\n============================="
        puts "\n\n"
        $cvObject configure Stage Scale $view_scale
        update_board
    }
        
    proc dragMessage { x y id} {
        tk_messageBox -message "giveMessage: $x $y $id"
        
    }
    
}

	
proc createGUI {} {	
        #
        ### -- G U I
        #
    variable w_textReport
        #
        #
    frame .f0 
    set f_canvas  [labelframe .f0.f_canvas   -text "board"  ]
    set f_config  [frame      .f0.f_config   ]

    pack  .f0      -expand yes -fill both
    pack  $f_canvas  $f_config    -side left -expand yes -fill both
    pack  configure  $f_config    -fill y

        #
        ### -- G U I - canvas 
    sketchboard::createStage    $f_canvas   1000 810  250 250 m  0.5 -bd 2  -bg white  -relief sunken

        #
        ### -- G U I - canvas demo
    set f_settings  [labelframe .f0.f_config.f_settings  -text "Test - Settings" ]
        
    labelframe  $f_settings.settings    -text "Settings"
    labelframe  $f_settings.arc         -text "large Arc Flag"
    labelframe  $f_settings.sweep       -text "Sweep Flag"
    labelframe  $f_settings.radius  	-text radius
    labelframe  $f_settings.length  	-text length
    labelframe  $f_settings.font    	-text font
    labelframe  $f_settings.demo    	-text demo
    labelframe  $f_settings.scale   	-text scale

    pack        $f_settings.settings	\
                $f_settings.arc		    \
                $f_settings.sweep		\
                $f_settings.radius		\
                $f_settings.length		\
                $f_settings.font		\
                $f_settings.demo		\
                $f_settings.scale   -fill x -side top 
                
        # x1, y1, rx, ry, angle, large_arc_flag, sweep_flag, x2, y2

    create_config_line $f_settings.settings.x1     "start  (x):"    ::sketchboard::x1     0 200
    create_config_line $f_settings.settings.y1     "start  (y):"    ::sketchboard::y1     0 200
    create_config_line $f_settings.settings.rx     "radius (x):"	::sketchboard::rx     0 200
    create_config_line $f_settings.settings.ry     "radius (y):"	::sketchboard::ry     0 200
    create_config_line $f_settings.settings.angle  "angle:     "	::sketchboard::angle  0 360
    create_config_line $f_settings.settings.x2     "end    (x):"    ::sketchboard::x2     0 200
    create_config_line $f_settings.settings.y2     "end    (y):"    ::sketchboard::y2     0 200

    radiobutton        $f_settings.arc.on \
                                -text      "   - Flag - 1 -   " \
                                -variable  "::sketchboard::large_arc_flag" \
                                -value     "1" \
                                -command   "::sketchboard::update_board"
    radiobutton        $f_settings.arc.off \
                                -text      "   - Flag - 0 -   " \
                                -variable  "::sketchboard::large_arc_flag" \
                                -value     "0" \
                                -command   "::sketchboard::update_board"
                                                                            
    radiobutton        $f_settings.sweep.on \
                                -text      "   - Flag - 1 -   " \
                                -variable  "::sketchboard::sweep_flag" \
                                -value     "1" \
                                -command   "::sketchboard::update_board"
    radiobutton        $f_settings.sweep.off \
                                -text      "   - Flag - 0 -   " \
                                -variable  "::sketchboard::sweep_flag" \
                                -value     "0" \
                                -command   "::sketchboard::update_board"
                                                                            

    create_config_line $f_settings.scale.cv_scale	" Canvas scale  "  sketchboard::cv_scale	 0.2  5.0  
                       $f_settings.scale.cv_scale.scl   configure       -resolution 0.1  -command "sketchboard::scale_board"
    button  		   $f_settings.scale.recenter   -text "recenter"    -command {sketchboard::recenter_board}
    button  		   $f_settings.scale.refit		-text "refit"       -command {sketchboard::refit_board}

    pack  	$f_settings.settings.x1 \
            $f_settings.settings.y1 \
            $f_settings.settings.rx \
            $f_settings.settings.ry \
            $f_settings.settings.angle \
            $f_settings.settings.x2 \
            $f_settings.settings.y2 \
         -side top  -fill x
         
    pack  	$f_settings.arc.off \
            $f_settings.arc.on \
         -side left  -fill x
         
    pack  	$f_settings.sweep.off \
            $f_settings.sweep.on \
         -side left  -fill x
         
    pack    $f_settings.scale.cv_scale \
            $f_settings.scale.recenter \
            $f_settings.scale.refit \
         -side left  -fill x		
                     
    pack  $f_settings  -side top -expand yes -fill both
        #
        ### -- G U I - canvas demo
    set f_demo  [labelframe .f0.f_config.f_demo     -text "Demo" ]
        button  $f_demo.bt_clear   -text "clear"    -command {$sketchboard::cvObject deleteContent} 
        button  $f_demo.bt_update  -text "update"   -command {sketchboard::update_board}
    pack  $f_demo  -side top 	-expand yes -fill x
        pack $f_demo.bt_clear 	-expand yes -fill x
        pack $f_demo.bt_update 	-expand yes -fill x
        
        #
        ### -- G U I - canvas status

        #
        ### -- G U I - canvas report
    set f_report  [labelframe .f0.f_config.f_report  -text "report" ]

                text  		   	   $f_report.text -width 50
                scrollbar 		   $f_report.sby -orient vert -command "$f_report.text yview"
                                   $f_report.text conf -yscrollcommand "$f_report.sby set"
                
    pack $f_report  -side top 	-expand yes -fill both
    pack $f_report.sby $f_report.text -expand yes -fill both -side right   

    set w_textReport $f_report.text  

    set f_export  [labelframe .f0.f_config.f_export  -text "export" ]
    pack $f_export  -side top   -expand yes -fill both  
}  
    #
createGUI
    #
    ####+### E N D
    #
update
    #
wm minsize . [winfo width  .]   [winfo height  .]






