 ########################################################################
 #
 # test_SVG.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2018/06/27
 #
 # 


    #
set sampleDirExternal   "C:/Program Files (x86)/Inkscape/share/examples"    
set sampleDirExternal   "C:/Program Files/Inkscape/share/examples"    
set sampleDirExternal   "C:/Program Files/Inkscape/share/inkscape/examples"    
    #
    #
set argv examples_path 
    #
set argv {file_path _sample/test_path_001.svg}
set argv {file_path _investigate/sram_red_etap_axs_003__crankset_new.svg}
set argv {file_path _investigate/sram_red_etap_axs_003__crankset_A.svg}
set argv {file_path "_investigate/shimano - FC-RX610-11-a-short.svg"}
set argv {file_path "_investigate/shimano - FC-RX610-11-a.svg"}
set argv {file_path "_investigate/test__matrix__rect__elipse.svg"}
    #
set argv {file_path "_sample/sram_red_etap_axs___frontderailleur.svg"}
set argv {file_path "_sample/sram_red_etap_axs___rearderailleur.svg"}
    #
# set argv {file_path "_investigate/paragon_DR208X_REV2.svg"}
# set argv {file_path "_investigate/path_001.svg"}
# set argv {file_path "_investigate/shimano - FC-RX610-11-b.svg"}
# set argv {file_path "_investigate/__example_path_multiple.svg"}
# set argv {file      _investigate/sram_red_etap_axs_003__crankset_new.svg}
# set argv {file      _sample/test_path_001.svg}
    #
# set argv examples_path 
    #
    
    
    
    #
puts "\n"
    #
set BASE_Dir  [file normalize [file dirname [file normalize $::argv0]]]
puts "   \$BASE_Dir ............... $BASE_Dir"
    #
set TEST_ROOT_Dir [file normalize [file dirname [lindex $argv0]]]
puts "   \$TEST_ROOT_Dir .......... $TEST_ROOT_Dir"
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
    #
set TEST_Sample_Dir [file join $TEST_ROOT_Dir _sample]
puts "    -> \$TEST_Sample_Dir .... $TEST_Sample_Dir"
    #
set TEST_Debug_Dir [file join $TEST_ROOT_Dir _debug]
puts "    -> \$TEST_Debug_Dir ..... $TEST_Debug_Dir"
    #
set TEST_Export_Dir [file join $TEST_ROOT_Dir __export]
puts "    -> \$TEST_Export_Dir .... $TEST_Export_Dir"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir ext-tcllib]
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
foreach dir $tcl_library {
    puts "      -> tcl_library $dir"
}    
    #
foreach dir $auto_path {
    puts "      -> auto_path   $dir"
}    
    #
    #
    # -- load package
    #
package require svgDOM 0.09
    #
    #
    #
proc xml2File {svgObject {filePrefix {}}} {
    variable TEST_Export_Dir
    puts "\n     -- xml2File --\n"
    puts "          -> \$TEST_Export_Dir ... $TEST_Export_Dir"
    puts "          -> \$svgObject ......... $svgObject"
        #
    set svgType [$svgObject pathType]
    puts "          -> \$svgType ........... $svgType"
        #
        #
    set svgXML  [$svgObject getSVG]
        #
    # puts "\n ---- Content ----\n"    
    # puts {<?xml version="1.0" encoding="UTF-8" standalone="no"?>}
    # puts [$svgXML asXML -doctypeDeclaration 1]
        #
    if {$filePrefix ne {}} {
        set fileName    [file join $TEST_Export_Dir [format {%s__%s.svg} $filePrefix $svgType]]
    } else {
        set fileName    [file join $TEST_Export_Dir [format {%s__%s.svg} [clock format [clock seconds] -format %Y_%m_%d] $svgType]]
    }
        #
    puts "          -> \$svgType    $svgType"
    puts "          -> \$fileName   $fileName"
       #
    set    fileContent {<?xml version="1.0" encoding="UTF-8" standalone="no"?>}
    append fileContent "\n" [$svgXML asXML -doctypeDeclaration 1]
        #
    if {[file exist $fileName]} {
        if {[file writable $fileName]} {
            set fp [open $fileName w]
            puts $fp $fileContent
            close $fp
            puts ""
            puts "         -- update ----------------------"
            puts "           ... write:"   
            puts "                       $fileName"
            puts "                   ... done"
        } else {
            puts "File: \n   $fileName\n  ... not writeable!"
        }
    } else {
            set fp [open $fileName w]
            puts $fp $fileContent
            close $fp
            puts ""
            puts "         -- new--------------------------"
            puts "           ... write:"  
            puts "                       $fileName "
            puts "                   ... done"
    }       
        #
    puts "\n     -- xml2File --\n"
        #
    return $fileName
        #
}
    #
puts "\n\n    ->  $argv0 $argv\n\n"    
    #
if {[info exists argv0] && $argv0 eq [info script]} {
    switch -exact -- [lindex $argv 0] {
        "examples" {
            foreach file [glob -join $sampleDirExternal *.svg] {
                puts $file
                puts "   -> $file"
                set svg {}
                catch {set svg [::svgDOM::SVG new file $file]} eID
                if {$svg ne {}} {
                    puts "$svg"
                    set filePrefix [file tail [file rootname $file]]
                    xml2File    $svg  $filePrefix
                }
            }
        }
        "examples_path" {
            foreach file [glob -join $sampleDirExternal *.svg] {
                puts $file
                puts "   -> $file"
                set svg {}
                catch {set svg [::svgDOM::SVG new file $file]} eID
                if {$svg ne {}} {
                    puts "$svg"
                    $svg pathType path
                    set filePrefix [file tail [file rootname $file]]
                    xml2File    $svg  $filePrefix
                }
            }
        }
        "tiger" {
            set file [file join $TEST_Sample_Dir Ghostscript_Tiger.svg]
            puts "   -> $file"
            set svg [::svgDOM::SVG new file $file]
                # $svg pathType fraction; # ... default
            xml2File    $svg
        }
        "tiger_path" {
            set file [file join $TEST_Sample_Dir Ghostscript_Tiger.svg]
            puts "   -> $file"
            set svg [::svgDOM::SVG new file $file]
            $svg pathType path
            xml2File    $svg         
        }
        "file" {
            set fileName    [file normalize [file join $BASE_Dir [lindex $argv 1]]]
            puts "\n        -> $fileName\n"
            set svg     [::svgDOM::SVG new file $fileName]
                    # $svg pathType fraction; # ... default
            xml2File    $svg
        }
        "file_path" {
            puts "           -> $BASE_Dir"
            puts "              {*}$argv"
            set fileName    [file normalize [file join $BASE_Dir [lindex $argv 1]]]
            puts "\n        -> $fileName\n"
            set svg     [::svgDOM::SVG new file $fileName]
            $svg pathType path
            # $svg pathType fraction
            set exportFile  [xml2File    $svg]
            puts "\n   ... \$exportFile $exportFile"
                # -----
            if 0 {
                set testDir     "C:/Users/manfred/rattleCAD_40/components/test"
                if {[file exist $testDir]} {
                    # check that it's a directory
                    if {[file isdirectory $testDir]} {
                        puts "$testDir exists"
                        file copy -force $exportFile $testDir
                    }
                }
            }
        }
        "debug" {
            set file [file join $TEST_Debug_Dir debug_001.svg]
            #set file [file join $TEST_Debug_Dir debug_003.svg]
            set file [file join $TEST_Debug_Dir debug_005.svg]
            #set file [file join $TEST_Debug_Dir debug_006.svg]
            #set file [file join $TEST_Debug_Dir debug_007.svg]
            #set file [file join $TEST_Debug_Dir debug_008.svg]
            #set file [file join $TEST_Debug_Dir debug_009.svg]
            #set file [file join $TEST_Debug_Dir debug_011.svg]
            #set file [file join $TEST_Debug_Dir debug_012_00.svg]
            #set file [file join $TEST_Debug_Dir debug_012_01.svg]
            set file [file join $TEST_Debug_Dir debug_013.svg]
            #set file [file join $TEST_Debug_Dir debug_014.svg]
            #set file [file join $TEST_Debug_Dir debug_014_01.svg]
            #set file [file join $TEST_Debug_Dir debug_014_99.svg]
            #set file [file join $TEST_Debug_Dir debug_014_04.svg]
            puts "   file ->\n$file"
            set svg     [::svgDOM::SVG new file $file]
            $svg pathType path
            # $svg report
            xml2File    $svg
        }
        default {
            # test - plug in an SVG constructor (e.g. file FILE.svg) and watch it work
            puts ""
            puts "  -- $argv0 -> $argv"
            puts ""
            puts "    usage:"
            puts ""
            puts "         examples"
            puts "              ... compute files out of \$sampleDirExternal"
            puts "                  ... $sampleDirExternal"
            puts "                  ... creates svg for tk::canvas"
            puts ""
            puts "         examples_path"
            puts "              ... compute files out of \$sampleDirExternal"
            puts "                  ... $sampleDirExternal"
            puts "                  ... creates svg for tkpath"
            puts ""
            puts "         tiger"
            puts "              ... compute _sample/Ghostscript_Tiger.svg"
            puts "                  ... creates svg for tk::canvas"
            puts ""
            puts "         tiger_path"
            puts "              ... compute _sample/Ghostscript_Tiger.svg"
            puts "                  ... creates svg for tkpath"
            puts ""
            puts "         file fileName"
            puts "              ... compute \$fileName"
            puts "                  ... creates svg for tk::canvas"
            puts ""
            puts "         file_path fileName"
            puts "              ... compute \$fileName"
            puts "                  ... creates svg for tkpath"
            puts ""
            puts "  -- $argv0 -> $argv"
            puts ""
        }
    }
}   
    #
    #
    #   
return
    #
    #
