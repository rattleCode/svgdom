 ########################################################################
 #
 # test_SVG.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2018/05/24
 #
 # 



set WINDOW_Title      "svgDOM, an extension for canvas"
    #
set BASE_Dir  [file normalize [file dirname [file normalize $::argv0]]]
puts "   \$BASE_Dir ............... $BASE_Dir"
    #
set TEST_ROOT_Dir [file normalize [file dirname [lindex $argv0]]]
# set TEST_ROOT_Dir [file normalize [file dirname $TEST_ROOT_Dir]]
puts "   \$TEST_ROOT_Dir .......... $TEST_ROOT_Dir"
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
    #
set TEST_Sample_Dir [file join $TEST_ROOT_Dir _sample]
puts "    -> \$TEST_Sample_Dir .... $TEST_Sample_Dir"
    #
set TEST_Export_Dir [file join $TEST_ROOT_Dir __export]
puts "    -> \$TEST_Export_Dir .... $TEST_Export_Dir"
    #
    #
    
    #
foreach dir $tcl_library {
    puts "   -> tcl_library $dir"
}
    #
set tcl_pkgPath $TEST_Library_Dir
foreach dir $tcl_pkgPath {
    puts "   -> tcl_pkgPath $dir"
}
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
foreach dir $auto_path {
    puts "   -> auto_path   $dir"
}
    #
    #
    # source [file join $BASE_Dir classSVG.tcl]
package require svgDOM 0.01
    #
    #
    
puts "\n ==================================================\n"    
    
    
return
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
    #
lappend auto_path "$TEST_Library_Dir/vectormath"
lappend auto_path "$TEST_Library_Dir/appUtil"
lappend auto_path "$TEST_Library_Dir/__ext_Libraries"
    #
lappend auto_path "$TEST_Library_Dir/lib-vectormath"
lappend auto_path "$TEST_Library_Dir/lib-tkpath0.3.3"
lappend auto_path "$TEST_Library_Dir/lib-appUtil"
    #
    #
package require   cad4tcl 0.01
package require   appUtil
    #
