svgDOM
=======================================

svgDOM reads a svg-file and creates an object structure build of tclOO objects 
corresponding to the xml structure in the svg-file. The very first 
implementation of svgDOM is build to sum up all transformation and style 
information along the xml-path to a graphic node like circle, rectangle, 
polygon, ... and also path and create a new svg-structure with all the style
and transformation information in the graphic node, which is now positioned
absolutely. In the same step all graphic elements are converted to polygons
or polylines - especially the path element. 
This new svg-structure is much easier to handle in the tk::canvas to display
the svg-content now.


svgDOM requires the following packages:
    TclOO                   ... part of tcl 8.6
    fileutil                ... part of tcllib
    math            1.2.5   ... part of tcllib
    math::geometry          ... part of tcllib
    tdom                    ... part of most tcl-distributions
    
    
if some packages are not available in your environment, you can download them
from here:
    https://sourceforge.net/projects/tcllib/

    
svgDOM is used in 
    simplifySVG as a part cad4tcl 
        ... https://sourceforge.net/projects/cad4tcl
    rattleCAD 
        ... https://sourceforge.net/projects/rattlecad
    
    

